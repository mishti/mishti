import React from 'react'
import Link from '@material-ui/core/Link'
import {makeStyles} from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Title from './Title'
import moment from 'moment'
import {useData} from '../Providers/DataProvider'
import {Patient} from '../models/Patient'
import {HealthCheck, HealthCheckPriority, HealthCheckPriorityLabel} from '../models/HealthCheck'
import {GridRowParams} from '@material-ui/data-grid'
import {useHistory, useParams} from 'react-router-dom'
import clsx from 'clsx'
import {Paper} from '@material-ui/core'
import Button from '@material-ui/core/Button'

function preventDefault(event) {
  event.preventDefault()
}

const useStyles = makeStyles(theme => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  normal: {
    background: '#c8e6c9',
  },
  warning: {
    background: '#ffcc80',
  },
  critical: {
    background: '#ffcdd2',
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}))

export default function PatientsTable({patients}) {
  const classes = useStyles()
  const history = useHistory()

  return (
    <Paper elevation={3} className={clsx(classes.paper)}>
      <div className={classes.toolbar}>
        <Title>Patients</Title>
        <Button
          variant="outlined"
          color="primary"
          onClick={() => {
            history.push('/register')
          }}
        >
          Add new patient
        </Button>
      </div>

      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Rank</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Number</TableCell>
            <TableCell>Currently Feeling</TableCell>
            <TableCell align="right">Oxygen Saturation</TableCell>
            <TableCell align="right">Heart Rate</TableCell>
            <TableCell align="right">Temperature</TableCell>
            <TableCell>First positivity</TableCell>
            <TableCell align="right">Days remaining</TableCell>
            <TableCell>Paramedic</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {patients.map(patient => {
            const healthChecks = patient.health_checks
            const healthCheck = healthChecks && healthChecks[healthChecks.length - 1]
            const daysRemaining = 14 - Math.ceil(patient.first_positive_test_date && ((new Date()).getTime() - patient.first_positive_test_date.getTime()) / (1000 * 3600 * 24))
            const statusLabel = HealthCheckPriorityLabel(patient.rank_status)
            const rowColor = {
              'GREEN': classes.normal,
              'AMBER': classes.warning,
              'RED': classes.critical,
            }[statusLabel]

            return (
              <TableRow className={rowColor} key={patient.id} onClick={(): void => {
                history.push(`/patient-record/` + patient.id)
              }}>
                <TableCell>{statusLabel}</TableCell>
                <TableCell>{patient.full_name}</TableCell>
                <TableCell>{patient.whatsapp_no}</TableCell>
                <TableCell>{healthCheck?.currently_feeling}</TableCell>
                <TableCell align="right">{healthCheck?.oxygen_saturation}</TableCell>
                <TableCell align="right">{healthCheck?.heart_rate}</TableCell>
                <TableCell align="right">{healthCheck?.temperature}</TableCell>
                <TableCell>{moment(patient.first_positive_test_date).format('MM/DD/YYYY')}</TableCell>
                <TableCell align="right">{daysRemaining}</TableCell>
                <TableCell align="right">{patient.assigned_volunteer}</TableCell>
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        <Link color="primary" href="#" onClick={preventDefault}>
          Load more patients
        </Link>
      </div>
    </Paper>
  )
}
