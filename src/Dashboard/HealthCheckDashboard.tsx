import React, {useEffect, useState} from 'react'
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import {getAllPatients, getPatient} from '../api'
import {Patient} from '../models/Patient'
import {PaperLayout} from '../PageLayouts/PaperLayout'
import PatientsTable from './PatientsTable';
import Content from './Content'
import Grid from '@material-ui/core/Grid'
import {HealthCheckHistory} from '../FormikForms/PatientRecordForm'
import {useParams} from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appToolbar: {
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 300,
  },
  balanceCard: {
    height: 200,
  },
}));

export function HealthCheckDashboard() {
  const classes = useStyles();
  // @ts-ignore
  const [ allPatients, setAllPatients ] = useState<Patient[] | null>(null);
  const [selectedPatientDetails, setSelectedPatientDetails] = useState<Patient | null>(null)
// @ts-ignore
  const {patientId} = useParams()

  useEffect(
    () => {
      async function getPatientAsync() {
        if (patientId === undefined) {
          return
        }

        const _patient = (await getPatient(patientId)) || undefined

        if (_patient !== undefined) {
          setSelectedPatientDetails(_patient)
        }
      }
      debugger;
      getPatientAsync()
    },
    [patientId],
  )

  if (!selectedPatientDetails) {
    return (
      <PaperLayout>
        <Typography variant="h2">Loading patient details...</Typography>
      </PaperLayout>
    )
  }

  return (
    <Content>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <HealthCheckHistory patient_id={selectedPatientDetails?.id} health_checks={selectedPatientDetails.health_checks}/>
        </Grid>
      </Grid>
    </Content>
  );
}
