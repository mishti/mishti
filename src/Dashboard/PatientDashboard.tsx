import React, {useEffect, useState} from 'react'
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import {getAllPatients, getPatient} from '../api'
import {Patient} from '../models/Patient'
import {PaperLayout} from '../PageLayouts/PaperLayout'
import PatientsTable from './PatientsTable';
import Content from './Content'
import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles((theme) => ({
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appToolbar: {
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 300,
  },
  balanceCard: {
    height: 200,
  },
}));

export function PatientDashboard() {
  const classes = useStyles();
  // @ts-ignore
  const [ allPatients, setAllPatients ] = useState<Patient[] | null>(null);

  useEffect(
    () => {
      async function getPatientsAsync() {
        const _patients = (await getAllPatients()) || undefined;

        if (_patients !== undefined) {
          setAllPatients(_patients);
        }
      }

      getPatientsAsync();
    },
    []
  );

  if (!allPatients) {
    return (
      <PaperLayout>
        <Typography variant="h2">Loading patient data...</Typography>
      </PaperLayout>
    );
  }

  return (
    <Content>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <PatientsTable patients={allPatients} />
        </Grid>
      </Grid>
    </Content>
  );
}
