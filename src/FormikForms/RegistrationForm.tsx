import React from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import FormikControl from "./FormikComponents/FormikControl";
import {Form, Formik} from "formik";
import * as yup from "yup";
import {PaperLayout} from "../PageLayouts/PaperLayout";
import {addPatient} from "../api";
import {Patient} from "../models/Patient";
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  form: {
    width: "100%", // Fix IE 11 issue.
  }
}));

export function RegistrationForm() {
  const classes = useStyles();
  const history = useHistory();
  const initialValues = {
    full_name: '',
    email: '',
    emergency_contact_name: '',
    emergency_contact_number: '',
    doctor_contact_number: '',
    dob: null,
    gender: '',
    whatsapp_no: '',
    home_address: '',
    referral_letter: '',
    first_positive_test_date: new Date(),
    symptoms: [],
    co_morbidities: [],
    terms_of_service: []
  }

  let RegistrationSchema = yup.object().shape({
    full_name: yup.string().required('This field is required'),
    email: yup.string().email('Please enter a valid email').required('This field is required'),
    dob: yup.date().typeError('Please enter a date in the format dd-mm-yyyy'),
    whatsapp_no: yup.number().required('This field is required'),
    home_address: yup.string().required('This field is required'),
    co_morbidities: yup.array().of(yup.string()).min(1).required('This field is required'),
    terms_of_service: yup
      .array()
      .of(yup.string())
      .length(1, 'Please select at least one item')
      .required('This field is required')
  });

  const onSubmit = (values, {setSubmitting}) => {
    console.log('Form data', values)
    console.log('Saved data', JSON.parse(JSON.stringify(values)))
    setSubmitting(true);

    addPatient(
      new Patient({
        full_name: values.full_name,
        emergency_contact_name: values.emergency_contact_name,
        emergency_contact_number: values.emergency_contact_number,
        doctor_contact_number: values.doctor_contact_number,
        dob: values.dob,
        gender: values.gender,
        whatsapp_no: values.whatsapp_no,
        email: values.email,
        home_address: values.home_address,
        referral_letter: values.referral_letter,
        first_positive_test_date: values.first_positive_test_date,
        blood_type: values.blood_type,
        symptoms: values.symptoms,
        co_morbidities: values.co_morbidities,
        terms_of_service: values.terms_of_service
      })
    ).then((patient) => {
      history.push('/health-check/' + patient.id);
    });
  }

  const genderOptions = [
    {value: 'male', label: 'Male'},
    {value: 'female', label: 'Female'},
    {value: 'other', label: 'Other'}
  ];

  const symptomOptions = [
    {value: 'dizziness', label: 'Dizziness or Tiredness - தலைச்சுற்றல் அல்லது சோர்வு'},
    {value: 'fever', label: 'Fever - காய்ச்சல்'},
    {value: 'shortnessOfBreath', label: 'Shortness of breath - மூச்சு திணறல்'},
    {value: 'soreThroat', label: 'Sore Throat'},
    {value: 'cough', label: 'Cough'},
    {value: 'other', label: 'Other'}
  ];

  const coMorbiditiesOptions = [
    {
      value: 'none',
      label: "No I don't have any existing co-morbidities- இல்லை என்னிடம் இருக்கும் இணை நோய்கள் எதுவும் இல்லை"
    },
    {value: 'hypertension', label: 'Hypertension (High BP)- உயர் இரத்த அழுத்தம் (உயர் பிபி)'},
    {value: 'angio', label: 'Angio (Heart condition)- ஆஞ்சியோ (இதய நிலை)'},
    {value: 'diabetes', label: 'Diabetes- நீரிழிவு நோய்'},
    {value: 'respiratory', label: 'Respiratory Problem'},
    {value: 'allergies', label: 'Allergies'},
    {value: 'other', label: 'Other'}
  ];

  const termOptions = [
    {value: 'self', label: 'I Agree - நான் ஒப்புக்கொள்கிறேன்'},
    {
      value: 'onBehalf',
      label: 'I Agree (on behalf of patient)- நான் ஒப்புக்கொள்கிறேன் (நோயாளியின் சார்பாக தன்னார்வலர்)'
    }
  ];

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={RegistrationSchema}
      onSubmit={onSubmit}
    >
      {({values, errors, touched, handleSubmit, handleChange, isSubmitting, setFieldValue}) => {
        return (
          <PaperLayout>
            <Form className={classes.form} noValidate onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={6} style={{textAlign: "center"}}>
                  <img src={"/images/covidNightingale.jpeg"} alt={"Covid Nightingale"} style={{width: "100%", maxWidth: "682px"}}/>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Typography variant="h4">
                    Patient Registration Form
                  </Typography>
                  <Typography variant="h5">
                    If you have been referred to this FREE Home Quarantine service by your doctor then please fill the
                    form
                    below to register. ( expected time to complete 7 minutes)
                  </Typography>
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='input'
                    label='Patient Name'
                    name='full_name'
                    helperText='(நோயாளியின் பெயர்): First name  Last name (முதல் பெயர் கடைசி பெயர்)'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='input'
                    label='Email address'
                    name='email'
                    helperText='(மின்னஞ்சல் முகவரி)'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='number'
                    label='Whatsapp Contact Number'
                    name='whatsapp_no'
                    helperText='(வாட்ஸ்அப் தொடர்பு எண்): Enter Whatsapp number on which the medical staff can easily message and call you.'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='date'
                    label='Date of Birth'
                    name='dob'
                    helperText='Please enter your date of birth'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='select'
                    label='Gender'
                    name='gender'
                    helperText=''
                    options={genderOptions}
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='textarea'
                    label='Home address'
                    name='home_address'
                    helperText='(வீட்டு முகவரி)'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='input'
                    label='Emergency Contact Name'
                    name='emergency_contact_name'
                    helperText='(அவசர பெயர்)'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='number'
                    label='Emergency Contact Number'
                    name='emergency_contact_number'
                    helperText='(அவசர தொடர்பு எண்)'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='number'
                    label="Doctor's Phone Number"
                    name='doctor_contact_number'
                    helperText="Enter the patient's doctor number if known for reference"
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='checkbox'
                    label='Do you suffer from any of these symptoms currently?'
                    name='symptoms'
                    helperText='தற்போது இந்த அறிகுறிகளில் ஏதேனும் பாதிக்கப்படுகிறீர்களா?'
                    options={symptomOptions}
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='checkbox'
                    label='Do you have any existing co-morbidities?'
                    name='co_morbidities'
                    helperText='உங்களிடம் ஏற்கனவே உள்ள ஏதேனும் நோய்கள் உள்ளதா?'
                    options={coMorbiditiesOptions}
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='date'
                    label='First day of symptoms'
                    name='first_positive_test_date'
                    helperText='Enter the date of first saw symptoms otherwise use the date you were tested COVID positive- முதலில் பார்த்த அறிகுறிகளின் தேதியை உள்ளிடவும் இல்லையெனில் நீங்கள் சோதிக்கப்பட்ட தேதியைப் பயன்படுத்தவும் COVID நேர்மறை'
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormikControl
                    control='checkbox'
                    label='I have read and understood the terms and conditions of this service and would like to be enrolled'
                    name='terms_of_service'
                    options={termOptions}
                    helperText='இந்த சேவையின் விதிமுறைகளையும் நிபந்தனைகளையும் நான் படித்து புரிந்து கொண்டேன், மேலும் சேர விரும்புகிறேன்.'
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button disabled={isSubmitting} size={'large'} color={"primary"} variant={"outlined"} type="submit" fullWidth>
                    Register
                  </Button>
                </Grid>
              </Grid>
            </Form>
          </PaperLayout>
        );
      }}
    </Formik>
  );
}
