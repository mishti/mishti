import React, {useEffect, useState} from 'react'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import {makeStyles} from '@material-ui/core/styles'
import FormikControl from './FormikComponents/FormikControl'
import {Form, Formik} from 'formik'
import * as yup from 'yup'
import {PaperLayout} from '../PageLayouts/PaperLayout'
import {getPatient, addHealthCheck} from '../api'
import {Patient} from '../models/Patient'
import {HealthCheck} from '../models/HealthCheck'
import {useHistory, useParams} from 'react-router-dom'
import {Paper, TableBody, TableCell, TableContainer, TableRow} from '@material-ui/core'
import orange from '@material-ui/core/colors/orange'
import {ContactDetails} from './PatientRecordForm'

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
  },
  idCard: {
    backgroundColor: orange[400],
  },
}))

export function HealthCheckForm() {
  const classes = useStyles()
  const history = useHistory();
  // @ts-ignore
  const {patientId} = useParams()
  const [selectedPatientDetails, setSelectedPatientDetails] = useState<Patient | null>(null)

  const initialValues = {
    breathing_difficulty: false,
    oxygen_saturation: '',
    heart_rate: '',
    temperature: '',
    new_symptoms: '',
    currently_feeling: '',
    medical_reports: [],
  }

  const breathingDifficultlyOptions = [
    {value: 'no', label: 'No'},
    {value: 'yes', label: 'Yes'},
  ]

  const currentlyFeelingOptions = [
    {value: 'better', label: 'Better'},
    {value: 'same', label: 'Same'},
    {value: 'worse', label: 'Worse'},
  ]

  let HealthCheckSchema = yup.object().shape({
    //TODO: provide ranges for medical fields: oxygen, heart rate, temperature
    oxygen_saturation: yup.number().typeError('Please enter a number between ...'),
    heart_rate: yup.number().typeError('Please enter a number between ...'),
    temperature: yup.number().typeError('Please enter a number between ...'),
    currently_feeling: yup.string().required('This field is required'),
  })

  const onSubmit = (values, {setSubmitting}) => {
    console.log('Form data', values)
    console.log('Saved data', JSON.parse(JSON.stringify(values)))
    setSubmitting(true)
    addHealthCheck(
      patientId, new HealthCheck({
        currently_feeling: values.currently_feeling,
        oxygen_saturation: values.oxygen_saturation,
        heart_rate: values.heart_rate,
        temperature: values.temperature,
        new_symptoms: values.new_symptoms,
      }),
    ).then((patient) => {
      console.log('Success!!!')
      setSubmitting(false)
      history.push('/health-check-dashboard/' + patientId);
    })
  }

  useEffect(
    () => {
      async function getPatientAsync() {
        if (patientId === undefined) {
          return
        }

        const _patient = (await getPatient(patientId)) || undefined

        if (_patient !== undefined) {
          setSelectedPatientDetails(_patient)
        }
      }

      getPatientAsync()
    },
    [patientId],
  )

  if (!selectedPatientDetails) {
    return (
      <PaperLayout>
        <Typography variant="h2">Loading patient details...</Typography>
      </PaperLayout>
    );
  }

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={HealthCheckSchema}
      onSubmit={onSubmit}
    >
      {({values, errors, touched, handleSubmit, handleChange, isSubmitting, setFieldValue}) => {
        return (
          <PaperLayout>
            <Form className={classes.form} noValidate onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={6} style={{textAlign: 'center'}}>
                  <img src={'/images/healthCheck.png'} alt={'Health check'} style={{width: '100%', maxWidth: '682px'}}/>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Typography variant="h4">
                    Patient Health Check Form
                  </Typography>
                  <Typography variant="h5">
                    Our doctors & nurses monitor your health data round the clock to make sure you need not stress about
                    how you are doing. They will guide you on the best course of action to keep you safe and well.
                    Together
                    we will defeat COVID!
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <ContactDetails selectedPatientDetails={selectedPatientDetails}/>
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='radio'
                    label='Breathing Difficulty?'
                    name='breathing_difficulty'
                    options={breathingDifficultlyOptions}
                    helperText='Are you experiencing shortness of breath or breathing difficulty compared to normal'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='radio'
                    label='How are you feeling compared to yesterday?'
                    name='currently_feeling'
                    options={currentlyFeelingOptions}
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <FormikControl
                    control='number'
                    label='Current Temperature'
                    name='temperature'
                    helperText='Use a thermometer to measure your accurate temperature.- உங்கள் துல்லியமான வெப்பநிலையை அளவிட ஒரு தெர்மோமீட்டரைப் பயன்படுத்தவும்.'
                  />
                </Grid>
                <Grid item xs={12} lg={8}>
                  <FormikControl
                    control='number'
                    label='SPO2 level (Oxygen saturation)'
                    name='oxygen_saturation'
                    helperText='Note image may be different to your oximeter. But you need to enter here what the reading for SpO2 is on your oximeter.- கீழே உள்ள குறிப்பு படம் உங்கள் ஆக்சிமீட்டருக்கு வேறுபட்டிருக்கலாம். ஆனால் உங்கள் ஆக்சிமீட்டரில் SpO2 க்கான வாசிப்பு என்ன என்பதை இங்கே உள்ளிட வேண்டும்.'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <img src={'/images/oxygenMeter.jpeg'} alt={'Oxygen saturation meter'}
                       style={{width: '100%', maxWidth: '300px'}}/>
                </Grid>
                <Grid item xs={12} lg={8}>
                  <FormikControl
                    control='number'
                    label='Current Heart Rate'
                    name='heart_rate'
                    helperText='Note image may be different to your oximeter. But you need to enter here what the reading for PRbpm is on your oximeter.-கீழே உள்ள குறிப்பு படம் உங்கள் ஆக்சிமீட்டருக்கு வேறுபட்டிருக்கலாம். ஆனால் உங்கள் ஆக்சிமீட்டரில் PRbpm க்கான வாசிப்பு என்ன என்பதை இங்கே உள்ளிட வேண்டும்.'
                  />
                </Grid>
                <Grid item xs={12} lg={4}>
                  <img src={'/images/heartMeter.jpeg'} alt={'Heart rate monitor'}
                       style={{width: '100%', maxWidth: '300px'}}/>
                </Grid>
                <Grid item xs={12}>
                  <FormikControl
                    control='textarea'
                    label='Any New Symptoms?'
                    name='new_symptoms'
                    helperText='List any new symptoms that you are experiencing below for e.g.loss of smell or taste, loose motions, dizziness, shivering, anxiety etc.'
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button disabled={isSubmitting} color={'primary'} variant={'outlined'} type="submit" fullWidth>
                    Submit new readings
                  </Button>
                </Grid>
              </Grid>
            </Form>
          </PaperLayout>
        )
      }}
    </Formik>
  )
}