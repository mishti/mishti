import React, {EventHandler, useEffect, useState} from 'react'
import clsx from 'clsx'

import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import {makeStyles} from '@material-ui/core/styles'
import FormikControl from './FormikComponents/FormikControl'
import {Form, Formik} from 'formik'
import * as yup from 'yup'
import {PaperLayout} from '../PageLayouts/PaperLayout'
import Content from '../Dashboard/Content'
import Title from '../Dashboard/Title'

import {addHealthCheck, addReview, getPatient} from '../api'
import {Patient} from '../models/Patient'
import {HealthCheck} from '../models/HealthCheck'
import {Review, ReviewStatus} from '../models/Review'
import {useHistory, useParams} from 'react-router-dom'
import {Paper, Table, TableBody, TableCell, TableHead, TableRow} from '@material-ui/core'
import moment from 'moment'
import firebase from 'firebase'

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  history: {
    flexGrow: 1,
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  formWrap: {
    position: 'sticky',
    top: 55,
    paddingBottom: theme.spacing(2),
  },
  form: {
    flexGrow: 1,
  },
  wordWrap: {
    whiteSpace: 'normal',
    wordWrap: 'break-word',
  },
  danger: {
    color: 'white',
    backgroundColor: 'red',
    '&:hover': {
      backgroundColor: 'red',
    },
  },
  hide: {
    display: 'none',
  }
}))

export function ContactDetails(props: { selectedPatientDetails: Patient | undefined }) {
  const classes = useStyles()
  if (props.selectedPatientDetails === undefined) return <></>
  return <Paper elevation={3} className={clsx(classes.paper, classes.history)}>
    <Title>Contact Details</Title>
    <Typography>
      <strong>Name:</strong> {props.selectedPatientDetails.full_name}
      <br/>
      <strong>Sex:</strong> {props.selectedPatientDetails.gender}
      <br/>
      <strong>Email:</strong> {props.selectedPatientDetails.email}
      <br/>
      <strong>Whatsapp:</strong> {props.selectedPatientDetails.whatsapp_no}
      <br/>
      <strong>Emergency Contact:</strong>{' '}
      {props.selectedPatientDetails.emergency_contact_name} ({props.selectedPatientDetails.emergency_contact_number})<br/>
      <strong>Address:</strong> {props.selectedPatientDetails.home_address}
      <br/>
    </Typography>
  </Paper>
}

export function HealthCheckHistory(props: { patient_id: string | null | undefined, health_checks: HealthCheck[] | undefined }) {
  const classes = useStyles()
  const history = useHistory()
  return <Paper elevation={3} className={clsx(classes.paper, classes.history)}>
    <div className={classes.toolbar}>

      <Title>Health Check History</Title>
      <Button
        variant="outlined"
        color="primary"
        onClick={() => {
          history.push('/health-check/' + props.patient_id)
        }}
      >
        Add another health check
      </Button>
    </div>
    <Table size="small">
      <TableHead>
        <TableRow>
          <TableCell className={classes.wordWrap}>Date</TableCell>
          <TableCell className={classes.wordWrap}>Currently Feeling</TableCell>
          <TableCell className={classes.wordWrap}>Heart Rate</TableCell>
          <TableCell className={classes.wordWrap}>Oxygen Saturation</TableCell>
          <TableCell className={classes.wordWrap}>Temperature</TableCell>
          <TableCell className={classes.wordWrap}>New Symptoms</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {
          props.health_checks && props.health_checks.map((check, key) => {
            return (
              <TableRow key={key}>
                <TableCell
                  className={classes.wordWrap}>{moment(check.created_at).format('DD/MM/YY hh:ss A')}</TableCell>
                <TableCell className={classes.wordWrap}>{check.currently_feeling}</TableCell>
                <TableCell className={classes.wordWrap}>{check.heart_rate}</TableCell>
                <TableCell className={classes.wordWrap}>{check.oxygen_saturation}</TableCell>
                <TableCell className={classes.wordWrap}>{check.temperature}</TableCell>
                <TableCell className={classes.wordWrap}>{check.new_symptoms}</TableCell>
              </TableRow>
            )
          }).reverse()
        }
      </TableBody>
    </Table>
  </Paper>
}

export function ReviewHistory(props: { reviews: Review[] | undefined, onClick }) {
  const classes = useStyles()
  const history = useHistory()
  return <Paper elevation={3} className={clsx(classes.paper, classes.history)}>
    <div className={classes.toolbar}>

      <Title>Review History</Title>
      <Button
        variant="outlined"
        color="primary"
        onClick={() => {
          props.onClick()
          console.log('add another review')
        }}
      >
        Add another review
      </Button>
    </div>


    <Table size="small">
      <TableHead>
        <TableRow>
          <TableCell className={classes.wordWrap}>Date</TableCell>
          <TableCell className={classes.wordWrap}>Review By</TableCell>
          <TableCell className={classes.wordWrap}>Notes</TableCell>
          <TableCell className={classes.wordWrap}>Status</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {props.reviews && props.reviews.map((review, key) => {
            return (
              <TableRow key={key}>
                <TableCell className={classes.wordWrap}>{moment(review.created_at).format('DD/MM/YY hh:ss A')}</TableCell>
                <TableCell className={classes.wordWrap}>{review.reviewer_display_name}</TableCell>
                <TableCell className={classes.wordWrap}>{review.notes}</TableCell>
                <TableCell className={classes.wordWrap}>{review.status}</TableCell>
              </TableRow>
            )
          },
        ).reverse()}
      </TableBody>
    </Table>
  </Paper>
}

function HealthDetails(props: { selectedPatientDetails: Patient | undefined }) {
  const classes = useStyles()
  if (props.selectedPatientDetails === undefined) return <></>
  return <Paper elevation={3} className={clsx(classes.paper, classes.history)}>
    <Title>Health Details</Title>
    <Typography>
      <strong>First Positive Test:</strong>{' '}
      {props.selectedPatientDetails.first_positive_test_date &&
      props.selectedPatientDetails.first_positive_test_date.toDateString()}
      <br/>
      <strong>Blood Type:</strong> {props.selectedPatientDetails.blood_type}
    </Typography>
  </Paper>
}

function ReviewForm(props: { selectedPatientDetails: Patient, disabled: boolean, onSubmit, setFieldValue, setDisplayReviewForm }) {
  const classes = useStyles()
  return <Paper elevation={3} className={clsx(classes.history, classes.paper)}>
    <Form noValidate onSubmit={props.onSubmit}>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <div className={classes.toolbar}>
            <Title> Review Patient {props.selectedPatientDetails.full_name}</Title>
            <Button
              variant="outlined"
              size={'small'}
              color="primary"
              onClick={() => {
                props.setDisplayReviewForm(false)
              }}
            >
              x
            </Button>
          </div>
        </Grid>
        <Grid item xs={12}>
          <FormikControl
            control="textarea"
            label="Review Notes"
            name="notes"
            helperText="Please comment on the status of the patient."
          />
        </Grid>
        <Grid item xs={12} md={3}>
          <Button
            disabled={props.disabled}
            color={'primary'}
            variant={'outlined'}
            type="submit"
            onClick={() => {
              console.log('save')
              props.setFieldValue('actionTaken', 'save')
            }}
            fullWidth
          >
            Save notes
          </Button>
        </Grid>
        <Grid item xs={12} md={3}>
          <Button
            disabled={props.disabled}
            variant={'outlined'}
            type="submit"
            className={classes.danger}
            fullWidth
            onClick={() => {
              console.log('escalate')
              props.setFieldValue('actionTaken', 'escalate')
            }}
          >
            Escalate to doctor
          </Button>
        </Grid>
        <Grid item xs={12} md={3}>
          <Button
            disabled={props.disabled}
            className={classes.danger}
            variant={'outlined'}
            type="submit"
            fullWidth
            onClick={() => {
              console.log('remove')
              props.setFieldValue('actionTaken', 'remove')
            }}
          >
            Remove patient
          </Button>
        </Grid>
      </Grid>
    </Form>
  </Paper>
}

export function PatientRecordForm({mode}) {
  //TODO: If mode is "doctor" or "volunteer" show appropriate UI

  const classes = useStyles()
  // @ts-ignore
  const {patientId} = useParams()
  const [selectedPatientDetails, setSelectedPatientDetails] = useState<Patient | null>(null)
  const [displayReviewForm, setDisplayReviewForm] = useState(false)
  const [toggleRefresh, setToggleRefresh] = useState(false)

  const initialValues = {
    notes: '',
    actionTaken: '',
  }

  const onSubmit = (values, {setSubmitting, resetForm}) => {
    console.log('Form data', values)
    console.log('Saved data', JSON.parse(JSON.stringify(values)))
    setSubmitting(true)
    const user = firebase.auth().currentUser

    function save() {
      console.log('save')
      addReview(
        patientId, new Review({
          notes: values.notes,
          reviewer_id: user?.uid,
          reviewer_display_name: user?.displayName,
          status: ReviewStatus.STABLE,
        }),
      ).then(() => {
        console.log('Success!!!')
        resetForm()
        setDisplayReviewForm(false)
        setToggleRefresh(!toggleRefresh)
        setSubmitting(false)
      })
    }

    function escalate() {
      console.log('save')
      addReview(
        patientId, new Review({
          notes: values.notes,
          reviewer_id: user?.uid,
          reviewer_display_name: user?.displayName,
          status: ReviewStatus.CRITICAL,
        }),
      ).then(() => {
        console.log('Success!!!')
        resetForm()
        setDisplayReviewForm(false)
        setToggleRefresh(!toggleRefresh)
        setSubmitting(false)
      })
    }

    function remove() {
      console.log('save')
      addReview(
        patientId, new Review({
          notes: values.notes,
          reviewer_id: user?.uid,
          reviewer_display_name: user?.displayName,
          status: ReviewStatus.DISCHARGED,
        }),
      ).then(() => {
        resetForm()
        setDisplayReviewForm(false)
        console.log('Success!!!')
        setToggleRefresh(!toggleRefresh)
        setSubmitting(false)
      })
    }

    // TODO: use values.actionTaken to update status and do any other escalation procedures
    switch (values.actionTaken) {
      case 'save':
        save()
        break
      case 'escalate':
        escalate()
        break
      case 'remove':
        remove()
        break
    }
  }

  useEffect(
    () => {
      async function getPatientAsync() {
        if (patientId === undefined) {
          return
        }

        const _patient = (await getPatient(patientId)) || undefined

        if (_patient !== undefined) {
          setSelectedPatientDetails(_patient)
        }
      }

      getPatientAsync()
    },
    [patientId, toggleRefresh],
  )

  if (!selectedPatientDetails) {
    return (
      <PaperLayout>
        <Typography variant="h2">Loading patient details...</Typography>
      </PaperLayout>
    )
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={yup.object().shape({
        notes: yup.string().required('This field is required'),
      })}
    >
      {({values, errors, touched, handleSubmit, handleChange, isSubmitting, setFieldValue}) => {
        return (
          <Content>
            <Grid container spacing={2}>
              <Grid item xs={12} className={clsx(classes.formWrap, displayReviewForm?'': classes.hide)}
              >
                <ReviewForm disabled={isSubmitting} onSubmit={handleSubmit}
                            selectedPatientDetails={selectedPatientDetails} setFieldValue={setFieldValue} setDisplayReviewForm={setDisplayReviewForm}/>
              </Grid>
              <Grid item xs={12} md={3}>
                <ContactDetails selectedPatientDetails={selectedPatientDetails}/>
              </Grid>
              <Grid item xs={12} md={3}>
                <HealthDetails selectedPatientDetails={selectedPatientDetails}/>
              </Grid>
              <Grid item xs={12}>
                <ReviewHistory reviews={selectedPatientDetails.reviews} onClick={()=>{setDisplayReviewForm(true)}}/>
              </Grid>
              <Grid item xs={12}>
                <HealthCheckHistory patient_id={selectedPatientDetails?.id}
                                    health_checks={selectedPatientDetails.health_checks}/>
              </Grid>
            </Grid>
          </Content>
        )
      }}
    </Formik>
  )
}