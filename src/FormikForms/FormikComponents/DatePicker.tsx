import React from 'react'
import {Field} from 'formik'
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers"
import DateFnsUtils from "@date-io/date-fns"

function DatePicker(props) {
  const {label, name, helperText} = props
  return (
    <Field name={name}>
      {({field, form}) => {
        const error = form.errors[name] && form.touched[name]
        const helper = error ? form.errors[name] : helperText
        const { value } = field
        const { setFieldValue } = form
        const disabled = form.isSubmitting

        return (
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker disableToolbar variant="inline" inputVariant="outlined"
                                error={error}
                                label={label}
                                disabled={disabled}
                                format="dd/MM/yyyy"
                                name={name}
                                value={value}
                                onChange={date =>setFieldValue(name, date)}
                                helperText={helper}
            />
          </MuiPickersUtilsProvider>
        )
      }}
    </Field>
  )
}

export default DatePicker;