import React from 'react'
import {Field} from 'formik'
import {
  FormControl,
  FormHelperText,
  RadioGroup as MuiRadioGroup, FormLabel, FormControlLabel, Radio
} from '@material-ui/core'

function RadioGroup(props) {
  const {label, name, helperText, options, ...rest} = props
  return (
    <Field name={name}>
      {({field, form}) => {
        const error = form.errors[name] && form.touched[name]
        const helper = error ? form.errors[name] : helperText
        const disabled = form.isSubmitting
        return (
          <FormControl variant="outlined" error={error} fullWidth>
            <FormLabel htmlFor={name}>{label}</FormLabel>
            <MuiRadioGroup
              row
              name={name}
              {...rest}
              {...field}
            >
              {
                options.map(
                  item => (
                    <FormControlLabel disabled={disabled} key={item.value} value={item.value} control={<Radio/>} label={item.label}/>
                  )
                )
              }

            </MuiRadioGroup>
            <FormHelperText>{helper}</FormHelperText>
          </FormControl>
        )
      }}
    </Field>
  )
}

export default RadioGroup;