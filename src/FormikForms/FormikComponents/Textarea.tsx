import React from 'react';
import { Field } from 'formik';
import { OutlinedInput, FormControl, FormHelperText, InputLabel } from '@material-ui/core';

function Textarea(props) {
	const { label, name, helperText, ...rest } = props;
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name] && form.touched[name];
				const helper = error ? form.errors[name] : helperText;
				const disabled = form.isSubmitting;
				return (
					<FormControl variant="outlined" error={error} fullWidth>
						<InputLabel htmlFor={name}>{label}</InputLabel>
						<OutlinedInput
							disabled={disabled}
							id={name}
							label={label}
							multiline
							{...rest}
							{...field}
						/>
						<FormHelperText>{helper}</FormHelperText>
					</FormControl>
				);
			}}
		</Field>
	);
}

export default Textarea;
