import React from 'react'
import {Field} from 'formik'
import {
  FormControl,
  FormHelperText,
  FormGroup,
  FormLabel,
} from '@material-ui/core'
import {CheckboxWithLabel} from "formik-material-ui";

function Checkbox(props) {
  const {label, name, helperText, options, ...rest} = props
  return (
    <Field name={name}>
      {({field, form}) => {
        const error = form.errors[name] && form.touched[name]
        const helper = error ? form.errors[name] : helperText
        return (
          <FormControl variant="outlined" error={error} fullWidth>
            <FormLabel htmlFor={name}>{label}</FormLabel>
            <FormHelperText>{helper}</FormHelperText>
            <FormGroup
              name={name}
              {...rest}
              {...field}
            >
              {
                options.map(
                  item => (
                    <Field
                      type="checkbox"
                      component={CheckboxWithLabel}
                      name={name}
                      key={item.value}
                      value={item.value}
                      Label={{
                        label: item.label
                      }}
                    />
                  )
                )
              }
            </FormGroup>
          </FormControl>
        )
      }}
    </Field>
  )
}

export default Checkbox;