import React from 'react'
import {Field} from 'formik'
import {
  FormControl,
  FormHelperText, InputLabel,
  Select as MuiSelect, MenuItem
} from '@material-ui/core'

function Select(props) {
  const {label, name, helperText, options, ...rest} = props
  return (
    <Field name={name}>
      {({field, form}) => {
        const error = form.errors[name] && form.touched[name]
        const helper = error ? form.errors[name] : helperText
        const disabled = form.isSubmitting
        return (
          <FormControl variant="outlined" error={error} fullWidth>
            <InputLabel htmlFor={name}>{label}</InputLabel>
            <MuiSelect
              disabled={disabled}
              id={name}
              label={label}
              {...rest}
              {...field}
            >
              <MenuItem value="">None</MenuItem>
              {
                options.map(
                  item => (<MenuItem key={item.value} value={item.value}>{item.label}</MenuItem>)
                )
              }
            </MuiSelect>
            <FormHelperText>{helper}</FormHelperText>
          </FormControl>
        )
      }}
    </Field>
  )
}

export default Select;