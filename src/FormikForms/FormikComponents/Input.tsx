import React from 'react'
import {Field} from 'formik'
import {
  OutlinedInput,
  FormControl,
  FormHelperText, InputLabel
} from '@material-ui/core'

function Input(props) {
  const {label, name, helperText, fullWidth, ...rest} = props
  return (
    <Field name={name}>
      {({field, form}) => {
        const error = form.errors[name] && form.touched[name]
        const helper = error ? form.errors[name] : helperText
        const disabled = form.isSubmitting
        return (
          <FormControl variant="outlined" error={error} fullWidth>
            <InputLabel htmlFor={name}>{label}</InputLabel>
            <OutlinedInput id={name} label={label} disabled={disabled} {...field} {...rest} />
            <FormHelperText>{helper}</FormHelperText>
          </FormControl>
        )
      }}
    </Field>
  )
}

export default Input;