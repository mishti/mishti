import {query, collection, where, add, limit, get, value, update, Doc, order} from 'typesaurus'
import { Patient, PatientStatus } from './models/Patient';
import {HealthCheck} from "./models/HealthCheck";
import Base from './models/Base';
import { Review } from './models/Review';

const patients = collection<Patient>('patients');

const wrapResult = <T extends Base>(Klass: { new(object): T }, result: Doc<T>): T => {
	if (!result){
		return result;
	}
	return new Klass({
		...result.data,
		id: result.ref.id,
	});
}

const wrapResults = <T extends Base>(Klass: { new(object): T }, results: Doc<T>[]): T[] => {
	return results && results.map(doc => wrapResult<T>(Klass, doc));
}


const unWrapModel = <T extends Base>(model: T) => {
	const {id, ...data} = model;
	return data;
}

const unWrapModels = <T extends Base>(models: T[]) => {
	return models.map(m => unWrapModel<T>(m));
}

const getIntakePatients = () => {
	// TODO: add doctor_id restriction
	return query(patients, [ where('status', '==', PatientStatus.Intake) ]);
};

const addPatient = (patient: Patient) => {
	return add(patients, unWrapModel<Patient>(patient));
};

const addHealthCheck = (patient_id: string, healthCheck: HealthCheck) => {
	return update(patients, patient_id, {
		rank_status: healthCheck.getOverallPriority(),
		latest_health_check: healthCheck.created_at,
		health_checks: value('arrayUnion', [unWrapModel<HealthCheck>(healthCheck)])
	})
};

const addReview = (patient_id: string, review: Review) => {
	return update(patients, patient_id, {
		reviews:  value('arrayUnion', [unWrapModel<Review>(review)]),
	})
};

const getAllPatients = async (): Promise<Patient[] | null> => {
	const patientDocs = await query(patients, [order('rank_status', 'desc'), limit(10)]);
	const docs: Patient[] =  wrapResults(Patient, patientDocs);
	return docs;
}

const getPatient = async (patient_id: string): Promise<Patient | null> => {
	const pDoc = await get(patients, patient_id);
	return pDoc && wrapResult(Patient, pDoc);
}

export { getIntakePatients, addPatient, getAllPatients, getPatient, addHealthCheck, addReview};
