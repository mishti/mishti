import { Grid, Typography } from "@material-ui/core";

const UrgentQueue = () => {
    return (
        <Grid container>
            <Typography variant="h1" component="h2">
                Urgent Queue
            </Typography>
        </Grid>
    );
};

export default UrgentQueue;
