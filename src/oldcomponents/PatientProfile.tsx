import React from 'react';
import { TableContainer, TableRow, TableBody, TableCell, Table, Paper } from '@material-ui/core';
import { Patient } from '../models/Patient';

function renderPatientProfile(patient: Patient) {
	return Object.entries(patient).filter(([ key, val ]) => val !== Object(val)).map(([ key, val ]) => (
		<TableRow>
			<TableCell component="th" scope="row">
				{key}
			</TableCell>
			<TableCell>{val}</TableCell>
		</TableRow>
	));
}

const PatientProfile = ({ patient }: { patient: Patient }) => {
	return (
		<div>
			<TableContainer component={Paper} style={{ width: '100%' }}>
				<Table>
					<TableBody>{renderPatientProfile(patient)}</TableBody>
				</Table>
			</TableContainer>
		</div>
	);
};

export default PatientProfile;
