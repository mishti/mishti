import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Paper, TableBody, TableCell, TableContainer, TableRow } from '@material-ui/core';

import { Formik, Form } from 'formik';
import * as yup from 'yup';

import { addPatient, getPatient } from '../api';
import { Patient } from '../models/Patient';
import HealthCheckFields, { HealthCheckValidationSchema } from './HealthCheckFields';
import Grid from '@material-ui/core/Grid';
import { HealthCheck } from '../models/HealthCheck';
import orange from '@material-ui/core/colors/orange';
import { useHistory, useParams } from 'react-router-dom';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			flexGrow: 1,
			padding: theme.spacing(3)
		},
		field: {
			width: '100%'
		},
		header: {
			marginBottom: theme.spacing(1)
		},
		form: {
			paddingTop: theme.spacing(3)
		},
		button: {
			backgroundColor: theme.palette.primary.main,
			color: theme.palette.primary.contrastText,
			'&:hover': {
				backgroundColor: theme.palette.primary.dark,
				color: theme.palette.primary.contrastText
			},
			'&:disabled': {
				backgroundColor: theme.palette.primary.dark,
				color: theme.palette.primary.contrastText,
				opacity: 0.6
			}
		},
		idCard: {
			backgroundColor: orange[400]
		}
	};
});

let RegistrationSchema = yup.object().shape({
	patient_id: yup.string().required('This field is required.'),
	email: yup.string().email('Please enter a valid email'),
	dob: yup.date().typeError('Please enter a date in the format yyyy-mm-dd'),
	aadhar_no: yup.string().required('This field is required'),
	home_address: yup.string().required('This field is required'),
	...HealthCheckValidationSchema
});

export default function RegistrationForm() {
	// @ts-ignore
	const { patientId } = useParams();
	const classes = useStyles();
	const history = useHistory();
	const [ selectedPatientDetails, setSelectedPatientDetails ] = useState<Patient | null>(null);

	useEffect(
		() => {
			async function getPatientAsync() {
				if (patientId === undefined) {
					return;
				}

				const _patient = (await getPatient(patientId)) || undefined;

				if (_patient !== undefined) {
					setSelectedPatientDetails(_patient);
				}
			}
			getPatientAsync();
		},
		[ patientId ]
	);

	return (
		<div className={classes.root}>
			<img src="/images/healthCheck.png" width={'100%'} alt="" />
			<h1 className={classes.header}>Patient Health Check Form</h1>
			<p>
				Our doctors & nurses monitor your health data round the clock to make sure you need not stress about how
				you are doing. They will guide you on the best course of action to keep you safe and well. Together we
				will defeat COVID!
			</p>
			<TableContainer component={Paper} className={classes.idCard}>
				<TableBody>
					<TableRow>
						<TableCell>Patient Id</TableCell>
						<TableCell>{patientId}</TableCell>
					</TableRow>
					<TableRow>
						<TableCell>Name</TableCell>
						<TableCell>{selectedPatientDetails ? selectedPatientDetails.full_name : ''}</TableCell>
					</TableRow>
					<TableRow>
						<TableCell>Date of Birth</TableCell>
						<TableCell>
							{selectedPatientDetails && selectedPatientDetails.dob ? (
								selectedPatientDetails.dob.toDateString()
							) : (
								''
							)}
						</TableCell>
					</TableRow>
				</TableBody>
			</TableContainer>

			<Formik
				initialValues={{
					breathing_difficulty: false,
					oxygen_saturation: null,
					heart_rate: null,
					temperature: null,
					new_symptoms: '',
					currently_feeling: '',
					medical_reports: []
				}}
				validationSchema={RegistrationSchema}
				onSubmit={(values, { setSubmitting }) => {
					setSubmitting(true);
					addPatient(
						new Patient({
							health_checks: [
								new HealthCheck({
									oxygen_saturation: values.oxygen_saturation,
									heart_rate: values.heart_rate,
									temperature: values.temperature,
									new_symptoms: values.new_symptoms,
									currently_feeling: values.currently_feeling,
									medical_reports: values.medical_reports
								})
							]
						})
					).then(() => {
						history.push(`/`);
					});
				}}
			>
				{({ values, errors, touched, handleSubmit, handleChange, isSubmitting, setFieldValue }) => {
					return (
						<Form className={classes.form} noValidate onSubmit={handleSubmit}>
							<Grid container spacing={3}>
								<HealthCheckFields
									handleChange={handleChange}
									values={values}
									errors={errors}
									touched={touched}
								/>
								<Grid item xs={12}>
									<Button disabled={isSubmitting} className={classes.button} type="submit">
										Register
									</Button>
								</Grid>
							</Grid>
						</Form>
					);
				}}
			</Formik>
		</div>
	);
}
