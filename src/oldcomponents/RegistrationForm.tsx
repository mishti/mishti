import React from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Button, TextField, FormLabel, FormControl, FormGroup, FormHelperText } from '@material-ui/core';
import RadioGroup from './controls/RadioGroup';
import DatePicker from './controls/DatePicker';

import DateFnsUtils from '@date-io/date-fns';

import { Formik, Form, Field } from 'formik';
import { CheckboxWithLabel } from 'formik-material-ui';
import * as yup from 'yup';

import { addPatient } from '../api';
import { Patient } from '../models/Patient';
import Grid from '@material-ui/core/Grid';
import { DropzoneArea } from 'material-ui-dropzone';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			flexGrow: 1,
			padding: theme.spacing(3)
		},
		field: {
			width: '100%'
		},
		header: {
			marginBottom: theme.spacing(1)
		},
		form: {
			paddingTop: theme.spacing(3)
		},
		button: {
			backgroundColor: theme.palette.primary.main,
			color: theme.palette.primary.contrastText,
			'&:hover': {
				backgroundColor: theme.palette.primary.dark,
				color: theme.palette.primary.contrastText
			},
			'&:disabled': {
				backgroundColor: theme.palette.primary.dark,
				color: theme.palette.primary.contrastText,
				opacity: 0.6
			}
		},
		dropzone: {
			minHeight: 'auto'
		}
	};
});

let RegistrationSchema = yup.object().shape({
	full_name: yup.string().required('This field is required'),
	dob: yup.date().typeError('Please enter a date in the format dd-mm-yyyy'),
	whatsapp_no: yup.number().required('This field is required'),
	home_address: yup.string().required('This field is required'),
	co_morbidities: yup.array().of(yup.string()).min(1).required('This field is required'),
	terms_of_service: yup
		.array()
		.of(yup.string())
		.length(1, 'Please select at least one item')
		.required('This field is required')
});

export default function RegistrationForm() {
	const classes = useStyles();
	const history = useHistory();
	const genderOptions = () => [
		{ id: 'male', title: 'Male' },
		{ id: 'female', title: 'Female' },
		{ id: 'other', title: 'Other' }
	];

	const symptomOptions = [
		{ value: 'dizziness', label: 'Dizziness or Tiredness - தலைச்சுற்றல் அல்லது சோர்வு' },
		{ value: 'fever', label: 'Fever - காய்ச்சல்' },
		{ value: 'shortnessOfBreath', label: 'Shortness of breath - மூச்சு திணறல்' },
		{ value: 'soreThroat', label: 'Sore Throat' },
		{ value: 'cough', label: 'Cough' },
		{ value: 'other', label: 'Other' }
	];

	const coMorbiditiesOptions = [
		{
			value: 'none',
			label: "No I don't have any existing co-morbidities- இல்லை என்னிடம் இருக்கும் இணை நோய்கள் எதுவும் இல்லை"
		},
		{ value: 'hypertension', label: 'Hypertension (High BP)- உயர் இரத்த அழுத்தம் (உயர் பிபி)' },
		{ value: 'angio', label: 'Angio (Heart condition)- ஆஞ்சியோ (இதய நிலை)' },
		{ value: 'diabetes', label: 'Diabetes- நீரிழிவு நோய்' },
		{ value: 'respiratory', label: 'Respiratory Problem' },
		{ value: 'allergies', label: 'Allergies' },
		{ value: 'other', label: 'Other' }
	];

	const termOptions = [
		{ value: 'self', label: 'I Agree - நான் ஒப்புக்கொள்கிறேன்' },
		{
			value: 'onBehalf',
			label: 'I Agree (on behalf of patient)- நான் ஒப்புக்கொள்கிறேன் (நோயாளியின் சார்பாக தன்னார்வலர்)'
		}
	];

	return (
		<div className={classes.root}>
			<img src="/images/registrationBanner.jpeg" width={'100%'} alt="" />

			<h1 className={classes.header}>Patient Registration</h1>
			<p>
				If you have been referred to this FREE Home Quarantine service by your doctor then please fill the form
				below to register. ( expected time to complete 7 minutes)
			</p>

			<Formik
				initialValues={{
					full_name: '',
					emergency_contact_name: '',
					emergency_contact_number: null,
					dob: null,
					gender: null,
					whatsapp_no: null,
					email: '',
					home_address: '',
					referral_letter: '',
					first_positive_test_date: new Date(),
					blood_type: '',
					symptoms: [],
					co_morbidities: [],
					terms_of_service: []
				}}
				validationSchema={RegistrationSchema}
				onSubmit={(values, { setSubmitting }) => {
					setSubmitting(true);
					addPatient(
						new Patient({
							full_name: values.full_name,
							emergency_contact_name: values.emergency_contact_name,
							emergency_contact_number: values.emergency_contact_number,
							dob: values.dob,
							gender: values.gender,
							whatsapp_no: values.whatsapp_no,
							email: values.email,
							home_address: values.home_address,
							referral_letter: values.referral_letter,
							first_positive_test_date: values.first_positive_test_date,
							blood_type: values.blood_type,
							symptoms: values.symptoms,
							co_morbidities: values.co_morbidities,
							terms_of_service: values.terms_of_service
						})
					).then((patient) => {
						// TODO: there's probably a better way to do redirects using react router?
						history.push('/health-check/' + patient.id);
					});
				}}
			>
				{({ values, errors, touched, handleSubmit, handleChange, isSubmitting, setFieldValue }) => {
					return (
						<Form className={classes.form} noValidate onSubmit={handleSubmit}>
							<Grid container spacing={3}>
								<Grid item xs={12}>
									<TextField
										type="text"
										label="Patient Name (நோயாளியின் பெயர்)"
										name="full_name"
										value={values.full_name}
										onChange={handleChange}
										error={!!(errors.full_name && touched.full_name)}
										helperText={
											touched.full_name ? (
												errors.full_name
											) : (
												'First name  Last name (முதல் பெயர் கடைசி பெயர்)'
											)
										}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<Grid item xs={12}>
									<TextField
										type="text"
										label="Emergency Contact Name - அவசர பெயர்"
										name="emergency_contact_name"
										value={values.emergency_contact_name}
										onChange={handleChange}
										error={!!(errors.emergency_contact_name && touched.emergency_contact_name)}
										helperText={
											touched.emergency_contact_name ? (
												errors.emergency_contact_name
											) : (
												'First name  Last name (முதல் பெயர் கடைசி பெயர்)'
											)
										}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<Grid item xs={12}>
									<TextField
										type="text"
										label="Emergency Contact Number - அவசர தொடர்பு எண்"
										name="emergency_contact_number"
										value={values.emergency_contact_number}
										onChange={handleChange}
										error={!!(errors.emergency_contact_number && touched.emergency_contact_number)}
										helperText={
											touched.emergency_contact_number ? (
												errors.emergency_contact_number
											) : (
												'Enter Whatsapp number on which the medical staff can easily message and call you.'
											)
										}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<Grid item xs={12}>
									<DatePicker
										utils={DateFnsUtils}
										name="dob"
										label="Date of Birth (DOB) - பிறந்த தேதி"
										value={values.dob}
										onChange={handleChange}
									/>
								</Grid>
								<Grid item xs={12}>
									<TextField
										type="text"
										label="Whatsapp Contact Number - வாட்ஸ்அப் தொடர்பு எண்"
										name="whatsapp_no"
										value={values.whatsapp_no}
										onChange={handleChange}
										error={!!(errors.whatsapp_no && touched.whatsapp_no)}
										helperText={
											touched.whatsapp_no ? (
												errors.whatsapp_no
											) : (
												'Enter Whatsapp number on which the medical staff can easily message and call you.'
											)
										}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<Grid item xs={12}>
									<RadioGroup
										name="gender"
										label="Gender"
										value={values.gender}
										onChange={(e) => {
											const { value } = e.target;
											setFieldValue('gender', value);
										}}
										items={genderOptions()}
									/>
								</Grid>
								<Grid item xs={12}>
									<TextField
										type="text"
										label="Email address- (மின்னஞ்சல் முகவரி)"
										name="email_address"
										value={values.email}
										onChange={handleChange}
										error={!!(errors.email && touched.email)}
										helperText={touched.email ? errors.email : ''}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<Grid item xs={12}>
									<TextField
										required
										type="text"
										label="Home address - (வீட்டு முகவரி)"
										name="home_address"
										value={values.home_address}
										onChange={handleChange}
										error={!!(errors.home_address && touched.home_address)}
										helperText={
											touched.home_address ? (
												errors.home_address
											) : (
												'Apartment or house number, city, post code, state - அபார்ட்மெண்ட் அல்லது வீட்டு எண், நகரம், அஞ்சல் குறியீடு, மாநிலம்'
											)
										}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<Grid item xs={12}>
									<div>
										<FormLabel>
											You can only enter this service upon referral from a doctor. Please upload
											picture of your doctors referral such as below.- மருத்துவரிடமிருந்து
											பரிந்துரைக்கப்பட்டால் மட்டுமே நீங்கள் இந்த சேவையை உள்ளிட முடியும். உங்கள்
											மருத்துவர்கள் பரிந்துரைக்கும் படத்தை கீழே பதிவேற்றவும்.
											<img src={'/images/docReferalLetter.png'} alt={'Doctor referral letter'} />
											<DropzoneArea
												dropzoneClass={classes.dropzone}

												// TODO: support file uploads
											/>
										</FormLabel>
									</div>
								</Grid>
								<Grid item xs={12}>
									<FormControl component="fieldset" style={{ display: 'flex' }}>
										<FormLabel component="legend">
											Do you suffer from any of these symptoms currently- தற்போது இந்த
											அறிகுறிகளில் ஏதேனும் பாதிக்கப்படுகிறீர்களா?
										</FormLabel>
										<FormGroup>
											{symptomOptions.map((opt) => (
												<Field
													type="checkbox"
													component={CheckboxWithLabel}
													name="symptoms"
													key={opt.value}
													value={opt.value}
													Label={{ label: opt.label }}
												/>
											))}
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid item xs={12}>
									<FormControl
										required
										error={!!(touched.co_morbidities && errors.co_morbidities)}
										component="fieldset"
										style={{ display: 'flex' }}
									>
										<FormLabel component="legend">
											Do you have any existing co-morbidities- உங்களிடம் ஏற்கனவே உள்ள ஏதேனும்
											நோய்கள் உள்ளதா?
										</FormLabel>
										<FormGroup>
											{coMorbiditiesOptions.map((opt) => (
												<Field
													type="checkbox"
													component={CheckboxWithLabel}
													name="co_morbidities"
													key={opt.value}
													value={opt.value}
													Label={{ label: opt.label }}
												/>
											))}
										</FormGroup>
										{!!(touched.co_morbidities && errors.co_morbidities) && (
											<FormHelperText>This field is required</FormHelperText>
										)}
									</FormControl>
								</Grid>
								<Grid item xs={12}>
									<div>
										<FormLabel component="legend">
											First day of symptoms or date you were tested COVID positive - அறிகுறிகளின்
											முதல் நாள் அல்லது நீங்கள் சோதிக்கப்பட்ட தேதி COVID நேர்மறை
										</FormLabel>
										<DatePicker
											utils={DateFnsUtils}
											name="first_positive_test_date"
											label=""
											value={values.first_positive_test_date}
											onChange={handleChange}
										/>
									</div>
								</Grid>
								<Grid item xs={12}>
									<FormControl
										error={!!(touched.terms_of_service && errors.terms_of_service)}
										component="fieldset"
										style={{ display: 'flex' }}
									>
										<FormLabel component="legend">
											I have read and understood the terms and conditions of this service and
											would like to be enrolled.- இந்த சேவையின் விதிமுறைகளையும் நிபந்தனைகளையும்
											நான் படித்து புரிந்து கொண்டேன், மேலும் சேர விரும்புகிறேன்.
										</FormLabel>
										<FormGroup>
											{termOptions.map((opt) => (
												<Field
													type="checkbox"
													component={CheckboxWithLabel}
													name="termsOfService"
													key={opt.value}
													value={opt.value}
													Label={{ label: opt.label }}
												/>
											))}
										</FormGroup>
										{!!(touched.terms_of_service && errors.terms_of_service) && (
											<FormHelperText>{errors.terms_of_service}</FormHelperText>
										)}
									</FormControl>
								</Grid>
								<Grid item xs={12}>
									<Button disabled={isSubmitting} className={classes.button} type="submit">
										Register
									</Button>
								</Grid>
								<Grid item xs={12}>
									<h3>Values</h3>
									<pre>{JSON.stringify(values, null, 2)}</pre>
								</Grid>
							</Grid>
						</Form>
					);
				}}
			</Formik>
		</div>
	);
}
