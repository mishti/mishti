import React from 'react';
import { Patient } from '../models/Patient';
import { Doc } from 'typesaurus';
import { getIntakePatients } from '../api';
import Alert from '@material-ui/lab/Alert';

interface Props {
}

interface State {
  patients: Doc<Patient>[]
}

class IntakeAlerts extends React.Component<Props, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            patients: []
        };
    }

  componentDidMount() {
      setInterval(() => {
        getIntakePatients().then((patients) => {
            this.setState({
                patients: patients
            });
        });
    }, 5000);
  }

  render() {
    return (
      <div className="Notices">
        {this.state.patients.map((patient) =>
            <Alert severity="warning">{patient.data.email}</Alert>
        )}
      </div>
    );
  }
}

export default IntakeAlerts;
