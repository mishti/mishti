import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, TextField } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import { Formik, Form } from 'formik';
import * as yup from 'yup';

import { addPatient } from '../api';
import { Patient } from '../models/Patient';
import HealthCheckFields, { HealthCheckValidationSchema } from './HealthCheckFields';
import Grid from '@material-ui/core/Grid';
import { HealthCheck } from '../models/HealthCheck';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			flexGrow: 1,
			padding: theme.spacing(3)
		},
		field: {
			width: '100%'
		},
		header: {
			marginBottom: theme.spacing(1)
		},
		form: {
			paddingTop: theme.spacing(3)
		},
		button: {
			backgroundColor: theme.palette.primary.main,
			color: theme.palette.primary.contrastText,
			'&:hover': {
				backgroundColor: theme.palette.primary.dark,
				color: theme.palette.primary.contrastText
			},
			'&:disabled': {
				backgroundColor: theme.palette.primary.dark,
				color: theme.palette.primary.contrastText,
				opacity: 0.6
			}
		}
	};
});

let RegistrationSchema = yup.object().shape({
	patient_id: yup.string().required('This field is required.'),
	email: yup.string().email('Please enter a valid email'),
	dob: yup.date().typeError('Please enter a date in the format yyyy-mm-dd'),
	aadhar_no: yup.string().required('This field is required'),
	home_address: yup.string().required('This field is required'),
	...HealthCheckValidationSchema
});

export default function RegistrationForm() {
	const classes = useStyles();
	const history = useHistory();

	return (
		<div className={classes.root}>
			<h1 className={classes.header}>Patient Intake Form</h1>
			<p>
				If you have been referred to this FREE Home Quarantine service by your doctor then please fill the form below to register.
				( expected time to complete 7 minutes)
			</p>

			<Formik
				initialValues={{
					full_name: '',
					dob: new Date(),
					aadhar_no: '',
					email: '',
					home_address: '',
					oxygen_saturation: 0,
					currently_feeling: 'better',
					new_symptoms_dizziness: '',
					new_symptoms_fever: '',
					new_symptoms_shortness: '',
					new_symptoms_other: '',
					heart_rate: 0,
					temperature: 0
				}}
				validationSchema={RegistrationSchema}
				onSubmit={(values, { setSubmitting }) => {
					setSubmitting(true);
					addPatient(
						new Patient({
							full_name: values.full_name,
							email: values.email,
							home_address: values.home_address,
							dob: values.dob,
							health_checks: [
								new HealthCheck({
									oxygen_saturation: values.oxygen_saturation,
									temperature: values.temperature,
									heart_rate: values.heart_rate,
									currently_feeling: values.currently_feeling,
									new_symptoms: ''
								})
							]
						})
					).then(() => {
						history.push(`/`);
					});
				}}
			>
				{({ values, errors, touched, handleSubmit, handleChange, isSubmitting, setFieldValue }) => {
					return (
						<Form className={classes.form} noValidate onSubmit={handleSubmit}>
							<TextField required type="text" label="Doctor ID" />
							<TextField required type="text" label="Full Name" />
							<TextField required type="text" label="Date of Birth" />
							<TextField required type="text" label="Biological Sex" />
							<TextField required type="text" label="Blood Type" />
							<TextField required type="text" label="Whatsapp Number" />
							<TextField required type="text" label="First Positive Test Date" />
							<TextField required type="text" label="Co-Morbidities" />

							<Grid container spacing={3}>
								<Grid item xs={12}>
									<TextField
										type="text"
										label="Full Name"
										name="full_name"
										value={values.full_name}
										onChange={handleChange}
										error={!!(errors.full_name && touched.full_name)}
										helperText={
											touched.full_name ? (
												errors.full_name
											) : (
												'Please do not modify if this is correct.'
											)
										}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<Grid item xs={12}>
									<MuiPickersUtilsProvider utils={DateFnsUtils}>
										<KeyboardDatePicker
											disableToolbar
											variant="inline"
											inputVariant="outlined"
											label="Date of Birth"
											format="M/dd/yyyy"
											openTo="year"
											name="dob"
											value={values.dob}
											error={!!(errors.dob && touched.dob)}
											helperText={'Please do not modify if this is correct.'}
											onChange={(value) => {
												setFieldValue('dob', value);
											}}
										/>
									</MuiPickersUtilsProvider>
								</Grid>
								<Grid item xs={12}>
									<TextField
										type="text"
										label="Email Address"
										name="email_address"
										value={values.email}
										onChange={handleChange}
										error={!!(errors.email && touched.email)}
										helperText={touched.email ? errors.email : ''}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<Grid item xs={12}>
									<TextField
										required
										type="text"
										label="Home Address"
										name="home_address"
										value={values.home_address}
										onChange={handleChange}
										error={!!(errors.home_address && touched.home_address)}
										helperText={
											touched.home_address ? (
												errors.home_address
											) : (
												'Apartment or house number, city, post code, state'
											)
										}
										className={classes.field}
										variant="outlined"
									/>
								</Grid>
								<HealthCheckFields
									handleChange={handleChange}
									values={values}
									errors={errors}
									touched={touched}
								/>
								<Grid item xs={12}>
									<Button disabled={isSubmitting} className={classes.button} type="submit">
										Register
									</Button>
								</Grid>
							</Grid>
						</Form>
					);
				}}
			</Formik>
		</div>
	);
}