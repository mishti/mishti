import { Grid, Typography } from "@material-ui/core";

const GreenQueue = () => {
    return (
        <Grid container>
            <Typography variant="h1" component="h2">
                Green Queue
            </Typography>
        </Grid>
    );
};

export default GreenQueue;
