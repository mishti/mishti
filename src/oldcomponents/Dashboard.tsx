import React from 'react';
import { DataGrid, GridRowParams } from '@material-ui/data-grid';

import { Patient } from "../models/Patient";
import { useHistory } from 'react-router';

const columns = [
    { field: 'created_at', headerName: 'Time', width: 120, type: 'date' },
    { field: 'patient_id', headerName: 'ID', width: 120 },
    { field: 'full_name', headerName: 'Name', width: 150 },
    { field: 'whatsapp_no', headerName: 'Whatsapp', width: 150 },
    { field: 'status', headerName: 'Status', width: 150 },
    { field: 'last_review', headerName: 'Review', width: 150 },
    { field: 'last_review_date', headerName: 'Review Time', width: 150, type: 'date' },
];


const Dashboard = ({
    patients,
}: {
    patients: Patient[],
}) => {
    const history = useHistory();

    return (
        <div style={{ height: 400, width: '100%' }}>
          <DataGrid rows={patients.map((p, i) => ({
              id: p.id,
              full_name: p.full_name,
              whatsapp_no: p.whatsapp_no,
              status: p.status,
              last_review: p.reviews ? p.reviews[p.reviews.length-1]?.notes : '',
              last_review_date: p.reviews ? p.reviews[p.reviews.length-1]?.created_at : '',
          }))}
          columns={columns}
          pageSize={10}
          onRowClick={(param: GridRowParams): void => {
              history.push(`/patient/${param.row.id}`);
          }}
          />
        </div>
      );
};


export default Dashboard;
