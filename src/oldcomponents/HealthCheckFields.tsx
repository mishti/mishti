import {
	TextField,
	InputLabel,
	RadioGroup,
	Radio,
	FormLabel,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormGroup,
	Checkbox
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { DropzoneArea } from 'material-ui-dropzone';
import * as yup from 'yup';
import Grid from '@material-ui/core/Grid';
import React from 'react';

const HealthCheckValidationSchema = {
	//TODO: provide ranges for medical fields: oxygen, heart rate, temperature
	oxygen_saturation: yup.number().typeError('Please enter a number between ...'),
	heart_rate: yup.number().typeError('Please enter a number between ...'),
	temperature: yup.number().typeError('Please enter a number between ...'),
	currently_feeling: yup.string().required('This field is required')
};

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1
	},
	field: {
		width: '100%'
	},
	dropzone: {
		maxWidth: 500
	},
	checkGroup: {
		marginTop: theme.spacing(1),
		marginBottom: theme.spacing(1)
	}
}));

const HealthCheckFields = ({ handleChange, values, errors, touched }) => {
	const classes = useStyles();

	return (
		<React.Fragment>
			<Grid item xs={12}>
				<TextField
					type="text"
					label="What is your current SPO2 level (Oxygen saturation)- உங்கள் தற்போதைய SPO2 நிலை என்ன (ஆக்ஸிஜன் செறிவு)"
					name="oxygen_saturation"
					onChange={handleChange}
					value={values.oxygen_saturation}
					error={!!errors.oxygen_saturation}
					helperText={
						touched.oxygen_saturation ? (
							errors.oxygen_saturation
						) : (
							'Note image below may be different to your oximeter. But you need to enter here what the reading for SpO2 is on your oximeter.- கீழே உள்ள குறிப்பு படம் உங்கள் ஆக்சிமீட்டருக்கு வேறுபட்டிருக்கலாம். ஆனால் உங்கள் ஆக்சிமீட்டரில் SpO2 க்கான வாசிப்பு என்ன என்பதை இங்கே உள்ளிட வேண்டும்.'
						)
					}
					className={classes.field}
					variant="outlined"
				/>
				<img src={'/images/oxygenMeter.jpeg'} alt={'SpO2 on Oxygen Meter'} width={'340px'} />
			</Grid>
			<Grid item xs={12}>
				<TextField
					type="text"
					label="Heart Rate"
					name="heart_rate"
					onChange={handleChange}
					value={values.heart_rate}
					error={!!errors.heart_rate}
					helperText={
						touched.heart_rate ? (
							errors.heart_rate
						) : (
							'Note image below may be different to your oximeter. But you need to enter here what the reading for PRbpm is on your oximeter.-கீழே உள்ள குறிப்பு படம் உங்கள் ஆக்சிமீட்டருக்கு வேறுபட்டிருக்கலாம். ஆனால் உங்கள் ஆக்சிமீட்டரில் PRbpm க்கான வாசிப்பு என்ன என்பதை இங்கே உள்ளிட வேண்டும்.'
						)
					}
					className={classes.field}
					variant="outlined"
				/>
				<img src={'/images/heartMeter.jpeg'} alt={'PRbpm on Oxygen Meter'} width={'340px'} />
			</Grid>
			<Grid item xs={12}>
				<TextField
					type="text"
					label="What is your current temperature- உங்கள் தற்போதைய வெப்பநிலை என்ன"
					name="temperature"
					onChange={handleChange}
					value={values.temperature}
					error={!!errors.temperature}
					helperText={
						touched.temperature ? (
							errors.temperature
						) : (
							"Use a thermometer to measure your accurate temperature.- உங்கள் துல்லியமான வெப்பநிலையை அளவிட ஒரு தெர்மோமீட்டரைப் பயன்படுத்தவும்."
						)
					}
					className={classes.field}
					variant="outlined"
				/>
			</Grid>
			<Grid item xs={12}>
				<FormControl required component="fieldset" className={classes.checkGroup}>
					<FormLabel component="legend">New Symptoms</FormLabel>
					<FormHelperText>
						List any new symptoms that you are experiencing below for e.g.loss of smell or taste, loose
						motions, dizziness, shivering, anxiety etc.
					</FormHelperText>
					<FormGroup>
						<FormControlLabel
							control={
								<Checkbox
									checked={values.new_symptoms_dizziness}
									onChange={handleChange}
									name="new_symptoms_dizziness"
								/>
							}
							label="Dizziness or Tiredness"
						/>
						<FormControlLabel
							control={
								<Checkbox
									checked={values.new_symptoms_fever}
									onChange={handleChange}
									name="new_symptoms_fever"
								/>
							}
							label="Fever"
						/>
						<FormControlLabel
							control={
								<Checkbox
									checked={values.new_symptoms_shortness}
									onChange={handleChange}
									name="new_symptoms_shortness"
								/>
							}
							label="Shortness of Breath"
						/>
						<TextField
							type="text"
							label="Other"
							name="new_symptoms_other"
							onChange={handleChange}
							value={values.new_symptoms_other}
						/>
					</FormGroup>
				</FormControl>
			</Grid>
			<Grid item xs={12}>
				<FormControl component="fieldset" className={classes.checkGroup}>
					<FormLabel component="legend">Currently Feeling</FormLabel>
					<FormHelperText>How are you feeling currently?</FormHelperText>
					<RadioGroup
						name="currently_feeling"
						onChange={handleChange}
						value={values.currently_feeling}
						defaultValue="better"
						className={classes.field}
					>
						<FormControlLabel value="better" control={<Radio />} label="Better" />
						<FormControlLabel value="same" control={<Radio />} label="Same" />
						<FormControlLabel value="worse" control={<Radio />} label="Worse" />
					</RadioGroup>
				</FormControl>
			</Grid>
			<Grid item xs={12}>
				<div className={classes.dropzone}>
					<FormLabel>
						Medical Reports
						<DropzoneArea

						// TODO: support file uploads
						/>
					</FormLabel>
				</div>
			</Grid>
		</React.Fragment>
	);
};

export { HealthCheckValidationSchema };

export default HealthCheckFields;
