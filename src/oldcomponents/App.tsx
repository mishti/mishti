import React, { Fragment, useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, useParams } from 'react-router-dom';
import Dashboard from './Dashboard';
import PatientRecord from './PatientRecord';
import Navbar from './Navbar';
import IntakeForm from './IntakeForm';
import RegistrationForm from './RegistrationForm';
import HealthCheckForm from './HealthCheckForm';
import GreenQueue from './GreenQueue';
import UrgentQueue from './UrgentQueue';
import CriticalQueue from './CriticalQueue';
import IntakeAlerts from './IntakeAlerts';
import './App.css';
import { Patient } from '../models/Patient';
import { HealthCheck } from '../models/HealthCheck';
import { Review, ReviewStatus } from '../models/Review';
import { Container } from '@material-ui/core';
import { getAllPatients, getPatient } from '../api';
import { makeStyles, CssBaseline, createMuiTheme, ThemeProvider, Paper } from '@material-ui/core';

import red from '@material-ui/core/colors/red';
import black from '@material-ui/core/colors/common';
import deepPurple from '@material-ui/core/colors/deepPurple';
import indigo from '@material-ui/core/colors/indigo';
import orange from '@material-ui/core/colors/orange';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#333996',
			light: '#3c44b126'
		},
		secondary: {
			main: '#f83245',
			light: '#f8324526'
		},
		background: {
			default: '#f4f5fd'
		}
	},
	overrides: {
		MuiAppBar: {
			root: {
				transform: 'translateZ(0)'
			}
		},
		MuiFormHelperText: {
			root: {
				color: black[0],
				'&$error': {
					color: red[700]
				},
				fontSize: '1rem'
			}
		},
		MuiFormLabel: {
			root: {
				color: indigo[900],
				fontWeight: 'bold',
				paddingBottom: '0.5rem'
			}
		},
		MuiTableContainer: {
			root: {
				width: 'fit-content'
			}
		},
		MuiFormControl: {
			root: {
				minWidth: '-webkit-fill-available'
			}
		}
	},
	props: {
		MuiIconButton: {
			disableRipple: true
		}
	}
});

const useStyles = makeStyles({
	appMain: {
		width: '100%'
	},
	pageContent: {
		margin: theme.spacing(5),
		padding: theme.spacing(3)
	},
	'input-label': {
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		width: '100%',
		color: 'red'
	},

	input: {
		'&::placeholder': {
			textOverflow: 'ellipsis !important',
			color: 'blue'
		}
	}
});

const dummyPatients = [
	new Patient({
		id: 'foobar',
		created_at: new Date(),
		full_name: 'Chakravarti Kulottunga Choladeva',
		health_checks: [
			new HealthCheck({
				oxygen_saturation: 98.0,
				temperature: 40.0,
				heart_rate: 150.0
			})
		],
		reviews: [
		]
	})
];

const PatientRecordContainer = () => {
	// @ts-ignore
	const { patientId } = useParams();
	const [ selectedPatientDetails, setSelectedPatientDetails ] = useState(dummyPatients[0]);

	useEffect(
		() => {
			async function getPatientAsync() {
				if (patientId === undefined) {
					return;
				}

				const _patient = (await getPatient(patientId)) || undefined;
				if (_patient !== undefined) {
					setSelectedPatientDetails(_patient);
				}
			}
			getPatientAsync();
		},
		[ patientId ]
	);

	return <PatientRecord patient={selectedPatientDetails} />;
};

const App = () => {
	const classes = useStyles();
	const [ patientList, setPatientList ] = useState(dummyPatients);

	useEffect(() => {
		async function getAllPatientsAsync() {
			const patients = await getAllPatients();
			console.log(patients);
			if (patients !== null) {
				setPatientList(patients);
			}
		}
		getAllPatientsAsync();
	}, []);

	return (
		<ThemeProvider theme={theme}>
			<Fragment>
				<Navbar />
				<Container maxWidth="lg">
					<Paper className={classes.pageContent}>
						<div className={classes.appMain}>
							<IntakeAlerts />
							<Switch>
								<Route path="/intake">
									<IntakeForm />
								</Route>
								<Route path="/registration">
									<RegistrationForm />
								</Route>
								<Route path="/health-check/:patientId">
									<HealthCheckForm />
								</Route>
								<Route path="/green-queue">
									<GreenQueue />
								</Route>
								<Route path="/urgent-queue">
									<UrgentQueue />
								</Route>
								<Route path="/critical-queue">
									<CriticalQueue />
								</Route>
								<Route path="/patient/:patientId">
									<PatientRecordContainer />
								</Route>
								<Route path="/">
									<Dashboard patients={patientList} />
								</Route>
							</Switch>
						</div>
					</Paper>
				</Container>
			</Fragment>
		</ThemeProvider>
	);
};

export default App;
