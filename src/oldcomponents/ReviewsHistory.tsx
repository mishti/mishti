import { DataGrid } from '@material-ui/data-grid';
import {Review} from "../models/Review";


const columns = [
    { field: 'created_at', headerName: 'Time', width: 150, type: 'date' },
    { field: 'notes', headerName: 'Notes', width: 300 },
    { field: 'doctor_id', headerName: 'Updated By', width: 150 },
    { field: 'status', headerName: 'Status', width: 150 },
];


const ReviewsHistory = ({
    reviews
}: {
    reviews: Review[]
}) => {
    return (
        <div style={{ height: 400, width: '100%' }}>
          <DataGrid rows={reviews.map((r, i) => ({
              ...r,
              id: i+1,
          }))} columns={columns} pageSize={5}/>
        </div>
      );
};

export default ReviewsHistory;
