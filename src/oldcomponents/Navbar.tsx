import React from "react";
import { AppBar, Toolbar, Typography, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {Link} from "react-router-dom";

const Navbar = () => {
    const classes = useStyles();
    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    Mishti
                </Typography>
                    <Button color="inherit"><Link className={classes.link} to="/">Home</Link></Button>
                    <Button color="inherit"><Link className={classes.link} to="/intake">Intake Form</Link></Button>
                    <Button color="inherit"><Link className={classes.link} to="/registration">Patient Registration Form</Link></Button>
                    <Button color="inherit"><Link className={classes.link} to="/health-check">Patient Health Check Form</Link></Button>
                    <Button color="inherit"><Link className={classes.link} to="/patient">Patient Record</Link></Button>
                    <Button color="inherit"><Link className={classes.link} to="/green-queue">Green Patient Queue</Link></Button>
                    <Button color="inherit"><Link className={classes.link} to="/urgent-queue">Urgent Patient Queue</Link></Button>
                    <Button color="inherit"><Link className={classes.link} to="/critical-queue">Critical Patient Queue</Link></Button>
            </Toolbar>
        </AppBar>
    );
};

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    link: {
        color: "white",
        textDecoration: "none",
    },
}));

export default Navbar;