import { TableContainer, TableRow, TableBody, TableCell, Paper, makeStyles } from "@material-ui/core";
import  {HealthCheck, HealthCheckPriority} from "../models/HealthCheck";


const useStyles = makeStyles({
    priority: (props: {
        priority?: HealthCheckPriority
    }) => ({
      background: getColourFromPriority(props.priority),
    }),
});

export function getColourFromPriority(priority: HealthCheckPriority | undefined): string{
    return (priority !== undefined) ? {
        [HealthCheckPriority.NORMAL] : '#c8e6c9',
        [HealthCheckPriority.WARNING] : '#ffcc80',
        [HealthCheckPriority.CRITICAL] : '#ffcdd2',
            }[priority] || '#e0e0e0' : '#e0e0e0';

}

const HealthCheckHeader = ({ healthCheckCols }) => {
    return (<TableRow>
        {
            healthCheckCols.map((h: string) =>
                <TableCell component="th" scope="col">{h}</TableCell>
            )
        }
    </TableRow>);
}

const HealthCheckCell = ({key, val, priority}: {key: string, val: any, priority: HealthCheckPriority | undefined}) => {
    const cellClasses = useStyles({
        priority: priority,
    });
    return <TableCell className={cellClasses.priority}>{val}</TableCell>;
}

const HealthCheckRow = ({healthCheck}: {healthCheck: HealthCheck}) => {
    return (
        <TableRow>
            {Object.entries(healthCheck).filter(([key, val]) => key !== 'patient_id').map(([key, val]) => {
                return <HealthCheckCell key={key} val={val} priority={healthCheck.getPriority(key)}/>
            })}
        </TableRow>
    );
}

const HealthCheckHistory = ({
    healthChecks
}: {
    healthChecks: HealthCheck[]
}) => {
    const healthCheckCols: string[] = [];
    healthChecks.forEach((healthCheck) =>
        Object.entries(healthCheck)
        .filter(([key, val]) => key !== 'patient_id' && key !== 'id')
        .forEach(([key, val]) => {
            if(!healthCheckCols.includes(key)){
                healthCheckCols.push(key);
            }
    }));

    return (
        <div>
            <TableContainer component={Paper}>
                <TableBody>
                    <HealthCheckHeader healthCheckCols={healthCheckCols} />
                    {healthChecks.map((healthCheck) => <HealthCheckRow healthCheck={healthCheck}/>)}
                </TableBody>
            </TableContainer>
        </div>
    );
};

export default HealthCheckHistory;
