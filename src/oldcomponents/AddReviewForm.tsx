import React, { useEffect, useState } from 'react';
import { Grid, TextField, Button, makeStyles } from '@material-ui/core';
import * as yup from 'yup';

import { Patient } from '../models/Patient';
import { Formik, Form, ErrorMessage } from 'formik';
import { Review, ReviewStatus } from '../models/Review';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => {
	return {
		row: {
			margin: '5px'
		},
		root: {
			flexGrow: 1
		},
		field: {
			width: '100%'
		},
		form: {
			paddingTop: theme.spacing(3)
		},
		button: {
			marginRight: '5px',
			'&:disabled': {
				opacity: 0.6
			}
		}
	};
});

const ReviewSchema = yup.object().shape({
	notes: yup.string().required('This field is required.')
});

const AddReviewForm = ({ patient }: { patient: Patient }) => {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Formik
				initialValues={{
					notes: '',
					status: undefined,
					form: ''
				}}
				validationSchema={ReviewSchema}
				onSubmit={(values, { setErrors }) => {
					console.log(values)
				}}
			>
				{({ values, errors, touched, isSubmitting, setFieldValue, handleChange }) => (
					<Form>
						{errors.form && <Alert severity="error">{errors.form}</Alert>}
						<Grid container>
							<Grid item xs={12} className={classes.row}>
								<TextField
									fullWidth
									multiline
									label="Notes"
									variant="outlined"
									name="notes"
									value={values.notes}
									onChange={handleChange}
									error={!!(errors.notes && touched.notes)}
									helperText={
										touched.notes ? errors.notes : 'Please comment on the status of the patient.'
									}
									className={classes.field}
									rows={4}
								/>
							</Grid>
							<Grid item xs={12} className={classes.row}>
								<Button
									disabled={isSubmitting}
									className={classes.button}
									variant="contained"
									color="primary"
									type="submit"
									onClick={() => {
										setFieldValue('status', ReviewStatus.STABLE);
									}}
								>
									Check In Later
								</Button>
								<Button
									disabled={isSubmitting}
									className={classes.button}
									variant="contained"
									color="secondary"
									type="submit"
									onClick={() => {
										setFieldValue('status', ReviewStatus.CRITICAL);
									}}
								>
									Critical Care
								</Button>
								<Button
									disabled={isSubmitting}
									className={classes.button}
									variant="outlined"
									color="primary"
									type="submit"
									onClick={() => {
										setFieldValue('status', ReviewStatus.UNAVAILABLE);
									}}
								>
									Unavailable
								</Button>
							</Grid>
						</Grid>
					</Form>
				)}
			</Formik>
		</div>
	);
};

export default AddReviewForm;
