import React from 'react';
import { Input, InputLabel, FormHelperText, Button } from '@material-ui/core';

class FormExample extends React.Component {
    constructor(props: any) {
        super(props);
        this.state = {
            validated: false
        };
    }

    handleSubmit = (event) => {
    //  const form = event.currentTarget;
    //   if (CheckboxValidity() === false) {
    //     event.preventDefault();
    //     event.stopPropagation();
    //   }

      this.setState({
          validated: true
      });
    };

    render() {
        return (
            <form noValidate
            // validated={this.state.validated}
            // onSubmit={this.handleSubmit}
            >
                    <InputLabel>First name2</InputLabel>
                    <Input
                        required
                        type="text"
                        placeholder="First name"
                        defaultValue="Mark"
                    />
                    <FormHelperText>Looks good!</FormHelperText>
                <Button type="submit">Submit form</Button>
            </form>
        );
    }
}

export default FormExample;