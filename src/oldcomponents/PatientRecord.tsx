import React from 'react';
import { Grid, Hidden, makeStyles, Paper, ThemeProvider, Typography } from '@material-ui/core';
import PatientProfile from './PatientProfile';
import AddReviewForm from './AddReviewForm';
import { Patient } from '../models/Patient';
import HealthCheckHistory, { getColourFromPriority } from './HealthCheckHistory';
import ReviewsHistory from './ReviewsHistory';
import { HealthCheckPriority } from '../models/HealthCheck';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			height: 'calc(100vh - 180px)'
		},
		header: (props: { priority?: HealthCheckPriority }) => ({
			background: getColourFromPriority(props.priority)
		}),
		section: {
			marginBottom: theme.spacing(4)
		},
		portal: {
			height: '45%',
			paddingTop: theme.spacing(3),

			overflowX: 'hidden',
			overflowY: 'scroll'
		},
		form: {
			height: '55%',
			overflowX: 'hidden',
			overflowY: 'scroll'
		}
	};
});

const PatientRecord = ({ patient }: { patient: Patient }) => {
	const patientName = patient.full_name;
	const healthChecks = patient.health_checks;
	const reviews = patient.reviews;
	const priority = healthChecks && healthChecks[healthChecks.length - 1].getOverallPriority();
	const classes = useStyles({
		priority: priority
	});

	//const patientPriorityColor = getColourFromPriority(healthChecks && healthChecks[healthChecks.length-1]?.getOverallPriority());
	return (
		<div className={classes.root}>
			<div className={classes.form}>
				<Typography component="h1" variant="h2" className={classes.header}>
					{patientName} - Condition: {priority}
				</Typography>
				<p>
					<strong>Patient ID:</strong> {patient.id} |
					<strong>DOB:</strong> {patient.dob ? patient.dob.toDateString() : 'Unknown'} |
					<strong>Gender:</strong> {patient.gender}
				</p>
				<AddReviewForm patient={patient} />
			</div>
			<div className={classes.portal}>
				<Typography variant="h3" component="h2">
					Review History
				</Typography>
				<Grid item xs={12} className={classes.section}>
					{reviews ? (
						<ReviewsHistory reviews={reviews} />
					) : (
						<p>
							<strong>There are no reviews yet.</strong>
						</p>
					)}
				</Grid>

				<Typography variant="h3" component="h2">
					Health Check History
				</Typography>
				<Grid item xs={12} className={classes.section}>
					<Paper>
						{healthChecks ? (
							<HealthCheckHistory healthChecks={healthChecks} />
						) : (
							<p>
								<strong>There are no health checks yet.</strong>
							</p>
						)}
					</Paper>
				</Grid>

				<Typography variant="h3" component="h2">
					Patient Profile
				</Typography>
				<Grid item xs={12} className={classes.section}>
					<PatientProfile patient={patient} />
				</Grid>
			</div>
		</div>
	);
};

export default PatientRecord;
