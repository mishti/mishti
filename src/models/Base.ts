class Base {
  id?: string | null = null;
  created_at: Date | null = null;

  constructor(obj){
    Object.assign(this, {
      ...obj
    });
    if (!this.created_at) {
      this.created_at = new Date()
    }
  }
}

export default Base;