
import Base from './Base';

export type Attributes = {

}

enum HealthCheckPriority {
  UNDEFINED = -1,
  NORMAL = 0,
  WARNING = 1,
  CRITICAL = 2
}

export function HealthCheckPriorityLabel(priority: HealthCheckPriority) {
  switch (priority) {
    case HealthCheckPriority.CRITICAL:
      return 'RED'
    case HealthCheckPriority.WARNING:
      return 'AMBER'
    default:
      return 'GREEN'
  }
}

function toFloat(val): number | null{
  return [undefined, null, ''].includes(val) ? null : parseFloat(val);
}

class HealthCheck extends Base {
  breathing_difficulty: boolean | null = null
  oxygen_saturation: number | null = null;
  heart_rate: number | null = null;
  temperature: number | null = null;
  new_symptoms: string | null = null;
  currently_feeling: string | null = null;
  medical_reports: string[] | null = null;

  constructor(obj: Partial<HealthCheck>) {
    super(obj);
    Object.assign(this, {
      ...obj,
      oxygen_saturation: toFloat(obj['oxygen_saturation']),
      heart_rate: toFloat(obj['heart_rate']),
      temperature: toFloat(obj['temperature']),
    });
  }

  getTemperaturePriority(): HealthCheckPriority{
    return this.temperature === null ? HealthCheckPriority.UNDEFINED :
                (this.temperature <= 38.0 ? HealthCheckPriority.NORMAL : HealthCheckPriority.WARNING);
  }

  getHeartRatePriority(): HealthCheckPriority{
    const heart_rate = this.heart_rate;
    return heart_rate === null ? HealthCheckPriority.UNDEFINED :
      (heart_rate <= 90.0 ? HealthCheckPriority.NORMAL :
        (90.0 < heart_rate && heart_rate <= 130.0 ? HealthCheckPriority.WARNING : HealthCheckPriority.CRITICAL));
  }

  getCurrentlyFeelingPriority(): HealthCheckPriority{
    const currently_feeling = this.currently_feeling;
    return currently_feeling === null ? HealthCheckPriority.UNDEFINED :
                (currently_feeling === 'worse' ? HealthCheckPriority.WARNING : HealthCheckPriority.NORMAL);
  }

  getBreathingDifficultPriority(): HealthCheckPriority{
    const currently_feeling = this.currently_feeling;
    return currently_feeling === null ? HealthCheckPriority.UNDEFINED :
      (currently_feeling === 'worse' ? HealthCheckPriority.WARNING : HealthCheckPriority.NORMAL);
  }

  getOxygenSaturationPriority(): HealthCheckPriority{
    return (this.breathing_difficulty === null) ? HealthCheckPriority.UNDEFINED :
      (this.breathing_difficulty ? HealthCheckPriority.WARNING : HealthCheckPriority.NORMAL)
  }

  getPriority(field: string): HealthCheckPriority | undefined {
    switch(field){
      case 'oxygen_saturation': return this.getOxygenSaturationPriority();
      case 'currently_feeling': return this.getCurrentlyFeelingPriority();
      case 'breathing_difficulty': return this.getBreathingDifficultPriority();
      case 'heart_rate': return this.getHeartRatePriority();
      case 'temperature': return this.getTemperaturePriority();
      default: return undefined;
    }
  }
  getOverallPriority(): HealthCheckPriority {
    return Math.max(
      this.getBreathingDifficultPriority(),
      this.getTemperaturePriority(),
      this.getHeartRatePriority(),
      this.getCurrentlyFeelingPriority(),
      this.getOxygenSaturationPriority()
    );
  }
}

export { HealthCheck, HealthCheckPriority };