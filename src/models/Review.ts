
import Base from './Base';

export type Attributes = {

}

enum ReviewStatus {
  STABLE = 'stable',
  CRITICAL = 'critical',
  UNAVAILABLE = 'unavailable',
  DISCHARGED = 'discharged',
}

class Review extends Base {
  notes: string | null = null;
  reviewer_id: string | null = null;
  reviewer_display_name: string | null = null;
  status: ReviewStatus | null = null;

  constructor(obj: Partial<Review>) {
    super(obj);
    Object.assign(this, {
      ...obj,
    });
  }

}

export { Review, ReviewStatus };