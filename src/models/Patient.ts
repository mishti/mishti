import Base from './Base'
import {HealthCheck, HealthCheckPriority} from './HealthCheck'
import {Review} from './Review'


export type Attributes = {};

enum PatientStatus {
  Intake = 'Intake',
  Assigned = 'Assigned',
  Escalated = 'Escalated',
  Discharged = 'Discharged',
}

class Patient extends Base {
  full_name?: string | null = null
  emergency_contact_name?: string | null = null
  emergency_contact_number?: number | null = null
  doctor_contact_number?: number | null = null
  dob?: Date | null = null
  gender?: string | null = null
  whatsapp_no?: number | null = null
  email?: string | null = null
  home_address?: string | null = null
  referral_letter?: string | null = null
  first_positive_test_date?: Date | null = null
  blood_type?: string | null = null
  symptoms?: string[] | null = null
  co_morbidities?: string[] | null = null
  status?: PatientStatus | null = null
  health_checks?: HealthCheck[]
  reviews?: Review[]
  terms_of_service?: string[] | null = null
  latest_health_check: Date | null = null
  latest_review: Date | null = null
  rank_status?: HealthCheckPriority
  assigned_volunteer?: string | null = null
  assignment_date?: Date | null = null
  discharge_date?: Date | null = null
  discharge_reason?: string | null = null

  constructor(obj: Partial<Patient>) {
    super({
      ...obj,
    })
    // not sure why I need to do this again.
    // is the this inside the parent class a new object?
    Object.assign(this, {
      ...obj,
      health_checks: obj.health_checks?.map((h, index) => new HealthCheck({...h, id: index.toString()})),
      reviews: obj.reviews?.map((r, index) => new Review({...r, id: index.toString()})),
    })
  }
}

export {
  Patient,
  PatientStatus,
}
