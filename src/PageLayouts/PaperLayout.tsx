import React from "react";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  main: {
    marginTop: theme.spacing(4),
  },
  paper: {
    margin: theme.spacing(8, 8),
    display: "flex",
    flexDirection: "column",
  },
}));

export function PaperLayout({children}) {
  const classes = useStyles();

  return (
    <Grid container component="main" className={classes.main}>
      <CssBaseline/>
      <Grid container justify="center">
        <Grid
          item
          sm={12}
          md={11}
          lg={10}
          xl={8}
          component={Paper}
          elevation={6}
        >
          <Grid className={classes.paper}>
            {children}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
