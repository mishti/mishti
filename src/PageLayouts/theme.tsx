import {useState} from "react";
import {createMuiTheme} from "@material-ui/core/styles";
import {indigo} from "@material-ui/core/colors";
import black from "@material-ui/core/colors/common";
import red from "@material-ui/core/colors/red";

export function useTheme() {
  const [currentTheme, setCurrentTheme] = useState({
    palette: {
      primary: indigo,
      secondary: indigo,
    },
  });
  const theme = createMuiTheme({
    ...currentTheme,
  });

  theme.overrides = {
    MuiList: {
      padding: {
        paddingTop: '0px',
      },
    },
    MuiButton: {
      // Name of the styleSheet
      root: {
        // Name of the rule
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.secondary.contrastText,
      },
      outlinedPrimary: {
        color: theme.palette.secondary.contrastText,
        '&:hover': {
          backgroundColor: theme.palette.secondary.main,
          color: theme.palette.primary.contrastText
        },
        '&:disabled': {
          backgroundColor: theme.palette.primary.dark,
          color: theme.palette.primary.contrastText,
          opacity: 0.6
        }
      }
    },
    MuiInputLabel: {
      outlined: {
        color: `${theme.palette.primary.dark}`,
      },
    },
    MuiOutlinedInput: {
      root: {
      }
    },
    MuiFormHelperText: {
      root: {
        color: black[0],
        '&$error': {
          color: red[700]
        },
        fontSize: '1rem'
      }
    },
    MuiFormLabel: {
      root: {
        color: `${theme.palette.primary.dark}`,
        fontWeight: 'bold',
        paddingBottom: '0.5rem'
      }
    },
    MuiTableContainer: {
      root: {
        width: 'fit-content'
      }
    },
    MuiFormControl: {
      root: {
        minWidth: '-webkit-fill-available'
      }
    }
  };

  return [theme, setCurrentTheme];
}
