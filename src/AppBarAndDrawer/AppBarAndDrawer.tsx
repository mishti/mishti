import React, {useState} from 'react'
import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import MenuIcon from '@material-ui/icons/Menu'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import {makeStyles, useTheme} from '@material-ui/core/styles'
import {Link as RouterLink, useLocation} from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import Avatar from '@material-ui/core/Avatar'
import Badge from '@material-ui/core/Badge'
import MailIcon from '@material-ui/icons/Mail'
import PalettePicker from '../Theme/PalettePicker'
import {Button} from '@material-ui/core'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import firebase from 'firebase'

export const drawerWidth = 240

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    paddingTop: '63px',
  },
  logo: {
    color: 'white',
    textDecoration: 'none',
  },
  appBar: {
    position: 'fixed',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  active: {
    backgroundColor: theme.palette.action.selected,
  },
  button: {
    margin: theme.spacing(1),
  },
}))

function signIn() {
  const provider = new firebase.auth.GoogleAuthProvider()
  firebase.auth()
    .signInWithPopup(provider)
    .then((result) => {
      /** @type {firebase.auth.OAuthCredential} */
      const credential = result.credential
      console.log('credential', credential)

      // This gives you a Google Access Token. You can use it to access the Google API.
      // @ts-ignore
      const token = credential.accessToken
      console.log('token', token)
      // The signed-in user info.
      const user = result.user
      console.log('user', user)
      window.location.reload();
      // ...
    }).catch((error) => {
    // Handle Errors here.
    const errorCode = error.code
    console.log('errorCode', errorCode)
    const errorMessage = error.message
    console.log('errorMessage', errorMessage)
    // The email of the user's account used.
    const email = error.email
    console.log('email', email)
    // The firebase.auth.AuthCredential type that was used.
    const credential = error.credential
    console.log('credential', credential)
    window.location.reload();
    // ...
  })
}

function signOut() {
  firebase.auth().signOut().then(() => {
    console.log('signed out')
    window.location.reload();
  }).catch((error) => {
    console.log('signed out error', error.toJSON())
    window.location.reload();
  });
}


function ResponsiveDrawer(props) {
  const {currentTheme, setCurrentTheme} = props
  const classes = useStyles()
  const theme = useTheme()
  const {pathname} = useLocation()
  const [mobileOpen, setMobileOpen] = React.useState(false)
  const [loginStatus, setLoginStatus] = React.useState('login')

  function handleAuth() {
    if (loginStatus === 'Hi') {
      signIn()
    } else {
      signOut()
    }
  }


  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      setLoginStatus('Bye ' + user.displayName + '!')
    } else {
      setLoginStatus('Hi')
    }
  });

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  /* Modifying the source code from the template example to use the react router pathname hook to set
  selected prop and to use the react router component prop */

  const drawer = (
    <div>
      <div className={classes.toolbar}/>
      <Divider/>
      <List>
        {[
          {text: 'home', icon: 'home'},
          {text: 'register', icon: 'registration'},
          {text: 'health-check', icon: 'health'},
          {text: 'login', icon: 'lock'},
          {text: 'profile', icon: 'person'},
          {text: 'dashboard', icon: 'dashboard'},
          {text: 'patient-dashboard', icon: ''},
          {text: 'health-check-dashboard', icon: ''},
          {text: 'pagelayout', icon: 'pagelayout'},
          {text: 'people', icon: 'people'},
          {text: 'map', icon: 'map'},
          {text: 'components', icon: 'apps'},
          {text: 'settings', icon: 'settings'},

        ].map(({text, icon}, index) => (
          <ListItem
            component={RouterLink}
            selected={pathname === `/${text}`}
            to={`/${text}`}
            button
            key={text}
          >
            <ListItemIcon>
              <Icon>{icon}</Icon>
            </ListItemIcon>
            <ListItemText primary={text.toUpperCase()}/>
          </ListItem>
        ))}
      </List>
      <Divider/>
    </div>
  )

  return (
    <div className={classes.root}>
      <CssBaseline/>
      <div
        style={{
          height: '64px',
          backgroundPosition: 'center',
          backgroundSize: 'cover',
          filter: 'contrast(75%)',
          position: 'absolute',
          top: '0px',
          width: '100%',
          zIndex: -2,
        }}
      />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography
            variant="h6"
            noWrap
            to={'/'}
            component={RouterLink}
            className={classes.logo}
          >
            Mishti
          </Typography>
          <div style={{flexGrow: 1}}></div>
          {/*
          <PalettePicker
            setCurrentTheme={setCurrentTheme}
            currentTheme={currentTheme}
          />
          <Badge badgeContent={4} color="primary">
            <MailIcon/>
          </Badge>
          */}
          <Button
            variant="contained"
            color="secondary"
            className={classes.button}
            startIcon={<AccountBoxIcon/>}
            onClick={handleAuth}
          >
            {loginStatus}
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default ResponsiveDrawer
