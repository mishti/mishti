import React, {useState, useEffect, ReactNode, useContext} from "react";
import { createContext } from "react";
import moment from "moment";

export const currentDay = moment();

let defaultData = {currentDay: {
        id: Math.floor(Math.random() * 1000),
        date: currentDay.add(1, "days").valueOf(),
        name: "Added Random",
        shipTo: "Location",
        paymentMethod: "Payment",
        amount: Math.floor(Math.random() * 3000 - 1000) -
            Math.floor(Math.random() * 100) / 100,
    }};

export type Data = typeof defaultData
export type SetData = (data: Data) => void
export const DataContext = createContext<
    { data: Data; setData: SetData; addRandomExpense: (data: Data, setData: SetData) => void } | undefined
    >(undefined);

function addRandomExpense(data: Data, setData: SetData) {
    return function () {
        const newDate = currentDay.add(1, "days").valueOf();
        const newMoney =
            Math.floor(Math.random() * 3000 - 1000) -
            Math.floor(Math.random() * 100) / 100;

        const newData = {
            id: Math.floor(Math.random() * 1000),
            date: newDate,
            name: "Added Random",
            shipTo: "Location",
            paymentMethod: "Payment",
            amount: newMoney,
        };

        const finalData: Data = {
            ...data,
            [newDate]: newData,
        };
        setData(finalData);
    };
}

for (let i = 0; i < 12; i++) {
    addRandomExpense(defaultData, (newData) => {
        defaultData = newData;
    })();
}

export function DataProvider({ children } : { children: ReactNode}) {
    const [data, setData] = useState(defaultData);

    useEffect(() => {}, []);
    return (
        <DataContext.Provider
            value={{
                data,
                setData,
                addRandomExpense: addRandomExpense(data, setData),
            }}
        >
            {children}
        </DataContext.Provider>
    );
}

export function useData() {
    const context = useContext(DataContext)
    if (!context) throw new Error('useData muse be inside a DataProvider')
    return context
}
