import React from "react";
import AppBarAndDrawer from "./AppBarAndDrawer/AppBarAndDrawer";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { SignIn } from "./SignIn";
import { Dashboard } from "./Dashboard/Dashboard";
import { Home } from "./Home/Home";
import { PaperLayout } from "./PageLayouts/PaperLayout";
import { ThemeProvider } from "@material-ui/core/styles";
import { useTheme } from "./PageLayouts/theme";
import { DataProvider } from "./Providers/DataProvider";

import Components from "./Components/Components";
import Settings from "./Settings/Settings";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {RegistrationForm} from "./FormikForms/RegistrationForm";
import {HealthCheckForm} from "./FormikForms/HealthCheckForm";
import {PatientRecordForm} from "./FormikForms/PatientRecordForm";
import {PatientDashboard} from './Dashboard/PatientDashboard'
import {HealthCheckDashboard} from './Dashboard/HealthCheckDashboard'


export default function App() {
  const [currentTheme, setCurrentTheme] = useTheme();
  return (
    <>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <ThemeProvider theme={currentTheme}>
            <DataProvider>
              <Router>
                <div>
                  <AppBarAndDrawer currentTheme={currentTheme} setCurrentTheme={setCurrentTheme} />
                  {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                  <Switch>
                    <Route path="/login">
                      <SignIn logout={''} login={''} loggedIn={''} />
                    </Route>
                    <Route path="/dashboard">
                      <Dashboard />
                    </Route>
                    <Route path="/health-check-dashboard/:patientId">
                      <HealthCheckDashboard />
                    </Route>
                    <Route path="/pagelayout">
                      <PaperLayout>
                      </PaperLayout>
                    </Route>
                    <Route path="/register">
                      <RegistrationForm />
                    </Route>
                    <Route path="/health-check/:patientId">
                      <HealthCheckForm />
                    </Route>
                    <Route path="/patient-record/:patientId">
                      <PatientRecordForm mode="volunteer" />
                    </Route>
                    <Route path="/patient-record-doctor/:patientId">
                      <PatientRecordForm mode="doctor" />
                    </Route>
                    <Route path="/components">
                      <Components />
                    </Route>
                    <Route path="/settings">
                      <Settings
                        currentTheme={currentTheme}
                        setCurrentTheme={setCurrentTheme}
                      />
                    </Route>
                    <Route path="/">
                      <PatientDashboard />
                    </Route>
                  </Switch>
                </div>
              </Router>
            </DataProvider>
        </ThemeProvider>
      </MuiPickersUtilsProvider>
    </>
  );
}
