import firebase from 'firebase'

const init = () => {
    const config = {
        apiKey: "AIzaSyDEG-fHqv3-UryTH08syzpJS7uUpgWxSS0",
        authDomain: "mishti-ea84b.firebaseapp.com",
        projectId: "mishti-ea84b",
        storageBucket: "mishti-ea84b.appspot.com",
        messagingSenderId: "569606212437",
        appId: "1:569606212437:web:9c5b1d771537773753fddc",
        measurementId: "G-X5K464DY13"
    };

    firebase.initializeApp(config);
    firebase.analytics();
    return firebase;
}

export { init, firebase };