import { query, collection, where } from 'typesaurus';
import { Patient, PatientStatus } from '../../models/Patient';

const patients = collection<Patient>('patients');

const getIntakePatients = () => {
    // TODO: add doctor_id restriction
    return query(patients, [where('status', '==', PatientStatus.Intake)]);
};

export {
    getIntakePatients
}