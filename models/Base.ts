class Base {
  created_at: Date | any = null;
  updated_at: Date | any = null;
}

export default Base;