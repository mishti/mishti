
import Base from './Base';

export type Attributes = {
  
}

class HealthCheck extends Base {
  oxygen_saturation: number | any = null;
  heart_rate: number | any = null;
  temperature: number | any = null;
  breathing_rate: number | any = null;
  new_symptoms: string | any = null;
  currently_feeling: string | any = null;
  medical_reports: string[] | any = null;
}

export default HealthCheck;
