import Base from './Base';


export type Attributes = {
  
};

enum PatientStatus {
  Intake = 'Intake'
}

class Patient extends Base {
  patient_id: string | any = null;
  full_name: string | any = null;
  dob: Date | any = null;
  aadhar_no: string | any = null;
  email_address: string | any = null;
  home_address: string | any = null;
  whatsapp_no: string | any = null;
  sex: string | any = null;
  blood_type: string | any = null;
  first_positive_test_date: Date | any = null;
  co_morbidities: string | any = null;
  status: PatientStatus | any = null;
}

export {
  Patient,
  PatientStatus
}
