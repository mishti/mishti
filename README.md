# MISHTI

## Requirements

### Node (npm) v12.18.3

## Getting Started

### Install Firebase Tools

- `npm install -g firebase-tools`
- `firebase login`

### Install Node Dependencies

- `npm install`

## Usage

### Debugging

- `npm run start`

### Running Tests

- `npm run test`

### Building

- `npm run build`

### Deploying to Firebase (Runs Build)

- `npm run deploy`